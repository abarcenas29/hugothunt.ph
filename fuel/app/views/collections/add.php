<!-- CONTENT -->	
<div id="content">
    <div id="feed" class="col-md-offset-1 col-md-10 col-xs-12">
        <!-- contains React component -->
        <div class="hugotForm">
            <h2>Create a Collection for Hugot themes!</h2>
            <form id="collectionSubmitForm" enctype="multipart/form-data">
                <input id="collectionName" type="text" placeholder="Name of Collection" required/>
                <input id="collectionDescription" class="hugotFormSource" type="text" placeholder="Description" />
                <input id="collectionCover" type="file" placeholder="Upload a cover photo" data-buttonBefore="true"/>
                <button type="submit" id="createCollectionBtn" class="ladda-button" data-style="expand-up" data-size="l">
                        CREATE THIS COLLECTION
                </button>
            </form>
        </div>
    </div>
    <div class="col-md-1 hidden-xs hidden-sm"></div>
</div>

 <!-- SIGN IN PROMPT -->
<div class="modal fade signinPopup" tabIndex="-1" role="dialog" aria-hidden="true">
  <div class="modal-dialog modal-sm">
    <div class="modal-content">
        <div class="modal-header">
            Please sign in first to create your collection. :)
        </div>
    </div>
  </div>
</div>

<script src="<?php print uri::create("assets/js/build/custom.js"); ?>"></script>
<script src="<?php print uri::create("assets/js/ladda.min.js"); ?>"></script>
<script src="<?php print uri::create("assets/js/spin.min.js"); ?>"></script>

<script>

	$('#collectionSubmitForm').submit(function(event) {
		event.preventDefault();
		var formData = new FormData($(this));
		formData.append('name', $('#collectionName').val());
		formData.append('photo', $('#collectionCover')[0].files[0]);
		formData.append('description', $('#collectionDescription').val());
		var l = Ladda.create( document.querySelector('#createCollectionBtn') );
		l.start();
		$.ajax({
		    url: "<?php print uri::create("api/collection/add.json"); ?>",
		    processData: false,
		    contentType: false,
		    type: 'POST',
		    data: formData,
		    success: function(data) {
		        console.log("success");
		        l.stop();
		        $("#createCollectionBtn").html('Done! Create pa!');
		        event.currentTarget.reset();
		        $(":file").filestyle('clear');
		    }.bind(this),
		    error: function(xhr, status, err) {

		    	$('.signinPopup').modal('show');
		    	l.stop();
		        console.error(this.url, status, err.toString());
		    }.bind(this)
		});
	});

</script>


