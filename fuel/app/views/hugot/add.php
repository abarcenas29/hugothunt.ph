<!-- CONTENT -->	
<div id="content">
    <div id="feed" class="col-md-offset-1 col-md-10 col-xs-12">
        <!-- contains React component -->
        <div class="hugotForm">
                <h2>Submit your Hugot line now!</h2>
                <form id="hugotSubmitForm" enctype="multipart/form-data">
                        <input id="hugotLine" type="text" placeholder="Hugot line" required/>
                        <input id="hugotSource" class="hugotFormSource" type="text" placeholder="Source (optional)" />
                        <input id="hugotMeme" type="file" placeholder="Attach meme here (optional)" data-buttonBefore="true"/>
                        <button type="submit" id="submitHugotBtn" class="ladda-button" data-style="expand-up" data-size="l">
                                SUBMIT NA THIS HUGOT</button>
                </form>
        </div>
    </div>
    <div class="col-md-1 hidden-xs hidden-sm"></div>
</div>

 <!-- SIGN IN PROMPT -->
<div class="modal fade signinPopup" tabIndex="-1" role="dialog" aria-hidden="true">
  <div class="modal-dialog modal-sm">
    <div class="modal-content">
        <div class="modal-header">
            Please sign in first to submit a hugot line. :)
        </div>
    </div>
  </div>
</div>

<script src="<?php print uri::create("assets/js/build/custom.js"); ?>"></script>
<script src="<?php print uri::create("assets/js/ladda.min.js"); ?>"></script>
<script src="<?php print uri::create("assets/js/spin.min.js"); ?>"></script>

<script>

    $('#hugotSubmitForm').submit(function(event) {
        event.preventDefault();
        var formData = new FormData($(this));
        formData.append('hugot', $('#hugotLine').val());
        formData.append('photo', $('#hugotMeme')[0].files[0]);
        formData.append('url', $('#hugotSource').val());
        var l = Ladda.create( document.querySelector('#submitHugotBtn') );
        l.start();
        $.ajax({
            url: "<?php print uri::create('api/hugot/add.json'); ?>",
            processData: false,
            contentType: false,
            type: 'POST',
            data: formData,
            success: function(data) {
                console.log("success");
                l.stop();
                $("#submitHugotBtn").html('Nabaon na ang hugot mo, submit pa ng mas malalim!');
                event.currentTarget.reset();
                $(":file").filestyle('clear');
            }.bind(this),
            error: function(xhr, status, err) {

                $('.signinPopup').modal('show');
                l.stop();
                console.error(this.url, status, err.toString());
            }.bind(this)
        });
    });
</script>

