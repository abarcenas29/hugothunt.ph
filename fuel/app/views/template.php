<!-- UPOU IS 226 - Final Project by Team 404 (Acosta, Barcenas, Dollosa) -->
<!DOCTYPE html>
<html>
    <head>
            <!--MAIN DEPENCIES-->
            <title>Hugot Hunt</title>

            <link rel="shortcut icon" type="image/png" href="faviconHH.ico"/>

            <?php 
                print Asset::css('bootstrap.css');
                print Asset::css('font-awesome.min.css');
                print Asset::css('bootstrap-social.css');
                print Asset::css('bootstrap-editable.css');
                print Asset::css('pace.css');
                print Asset::css('http://fonts.googleapis.com/css?family=Fira+Sans:300,400,700');
                print Asset::css('hugothunt.css');
                
                print Asset::js('react-0.12.1.js');
                print Asset::js('jquery-2.1.3.min.js');
                print Asset::js('jquery.cookies.js');
                print Asset::js('bootstrap.js');
                print Asset::js('bootstrap-filestyle.min.js');
                print Asset::js('bootstrap-editable.js');
                print Asset::js('ladda.min.js');
                print Asset::js('spin.min.js');
                print Asset::js('pace.min.js');
            ?>
            <link rel="stylesheet" href="<?php print uri::create("assets/js/ladda-themeless.min.css"); ?>">
        <!--END OF MAIN DEPENDECIES-->
    </head>
    <body>

        <script>
        var base_url = "<?php print Uri::create('api/'); ?>";
        function statusChangeCallback(response) {
            console.log('statusChangeCallback');
            console.log(response);
            if (response.status === 'connected') {
              hugotAuthAPI();
            } else if (response.status === 'not_authorized') {
                document.getElementById('status').innerHTML = 'Please log ' +
                'into this app.';
            } else {
                document.getElementById('status').innerHTML = 'Please log ' +
                'into Facebook.';
            }
        }

        function checkLoginState() {
            FB.getLoginStatus(function(response) {
              statusChangeCallback(response);
            });
        }

        window.fbAsyncInit = function() {
            FB.init({
              appId      : '900618836666202',
              xfbml      : true,
              version    : 'v2.3'
            });

            FB.getLoginStatus(function(response) {
                statusChangeCallback(response);
              });
        };

        (function(d, s, id) {
            
            var js, fjs = d.getElementsByTagName(s)[0];
            if (d.getElementById(id)) {return;}
            js = d.createElement(s); js.id = id;
            js.src = "//connect.facebook.net/en_US/sdk.js#xfbml=1&version=v2.3&appId=900618836666202";
            fjs.parentNode.insertBefore(js, fjs);
            
            }(document, 'script', 'facebook-jssdk'));

        function hugotAuthAPI() {
                FB.api('/me', function(response) {
                console.log('Successful login for: ' + response.name);
                    var formData = new FormData();
                    formData.append('social_id', response.id);
                    formData.append('social_network', "facebook");
                    formData.append('email', response.email);
                    formData.append('name', response.name);

                    $.ajax({
                        url: base_url + "user/authenticate.json",
                        processData: false,
                        contentType: false,
                        type: 'POST',
                        data: formData,
                        success: function(data) {
                            console.log("success fb login");
                            var isLoggedIn = $.cookie('isLoggedIn');
                            if(typeof isLoggedIn === 'undefined'){
                                window.location.href = '/';
                                $.cookie('isLoggedIn',true);
                            }

                        }.bind(this),
                        error: function(xhr, status, err) {
                            console.error(this.url, status, err.toString());
                        }.bind(this)
                    });


                });
        }

        function hugotOutAPI() {
            $.ajax({
                url: base_url + "user/logout.json",
                type: 'POST',
                success: function(data) {
                    console.log("success logout");
                    location.reload();
                }.bind(this),
                error: function(xhr, status, err) {
                    console.error(this.url, status, err.toString());
                }.bind(this)
            });
        }

        var signed_in = <?php echo json_encode(Session::get('user_id', null)); ?>;
        console.log("sign: " + signed_in);
    
        </script>

    <?php if(!isset($disable_navigation)): ?>
        <!-- NAVIGATION -->
        <div id="nav" role="navigation" class="navbar">
                <div id="logo" class="navbar-brand">
                        <a class="" href="<?php print uri::create("/"); ?>"><img src="<?php print uri::create("assets/img/logo.png"); ?>"></a>
                </div>
                <div class="col-md-4 col-sm-6 col-xs-8" id="title">
                        <span id="nav-title"><h1>Hugot Hunt</h1></span>
                        <span id="nav-tagline"><h4>Tambayan ng mga may pinaglalaban</h4></span>
                </div>
                <ul id="nav-main" class="nav navbar-nav navbar-right hidden-sm hidden-xs">
                        
                        <?php if(is_null(Session::get('user_id',null))): ?>
                        <!-- Not Logged In -->
                        <div class="fb-login-button" data-max-rows="2" data-size="large" data-show-faces="false" data-auto-logout-link="true"></div>

                        <?php else : ?>
                        <!-- User Logged In -->
                            <div>
                                <span class="loggedMsg1"><?php print Session::get('name') ?></span><span class="loggedMsg2">, daming hugot.</span>
                                <span id="hugotLogOut"><i class="fa fa-sign-out"></i>LOGOUT?</span>
                            </div>
                        <?php endif;?>

                        <li><a class='navHome' href="<?php print uri::create("/"); ?>"><span class="glyphicon glyphicon-home" aria-hidden="true"></span>HOME</a></li>
                        <li><a class='navCollections' href="<?php print uri::create("collections/"); ?>"><span class="glyphicon glyphicon-book" aria-hidden="true"></span>COLLECTIONS</a></li>
                        <li><a class='navSubmit' href="<?php print uri::create("hugot/add"); ?>"><button type="button" class="btn" ><span class="glyphicon glyphicon-plus" aria-hidden="true"></span>SUBMIT</button></a></li>
                </ul>
        </div>
    <?php endif; ?>
    <!--END OF NAVIGATION-->

    <?php print $content; ?>

    <!-- FOOTER -->
    <div id="footer">
    </div>


	
    <!--IDENTITY-->
    <script>


        $('#hugotLogIn').click(function () {
            FB.login();
            hugotAuthAPI();
        });

        $('#hugotLogOut').click(function () {
            FB.logout(function(response) {
                console.log("fb logout");
            });
            hugotOutAPI();

        });

        $('#detailsModal').on('hide.bs.modal', function (e) {
          window.history.back();
        });

    </script>

    </body>
</html>