<!-- CONTENT -->	
<div id="content">
    <div id="user" class="col-md-offset-1 col-md-10 col-xs-12">
		<!-- user.js component -->
    </div>
    <div class="col-md-1 hidden-xs hidden-sm"></div>
</div>
<script>
	var url = window.location.href;
	var id = url.replace(/^.*\/|\.[^.]*$/g, '');
	
	$.fn.editable.defaults.mode = 'inline';

</script>

<script src="<?php print uri::create("assets/js/build/user.js"); ?>"></script>
<script src="<?php print uri::create("assets/js/build/custom.js"); ?>"></script>

