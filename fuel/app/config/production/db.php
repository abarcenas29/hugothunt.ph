<?php
/**
 * The production database settings. These get merged with the global settings.
 */

return array(
    // a MySQL driver configuration
    'default' => array(
        'type'		  => 'mysqli',
        'connection'  => array(
                'hostname'   => 'localhost',
                'username'   => 'hugothunt',
                'password'   => 'SM6QSAUAvcKZzNC2',
                'port'       => '3306',
                'database'   => 'hugothunt'
        ),
        'profiling' => false
    )
);
