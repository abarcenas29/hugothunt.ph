<?php
/**
 * The development database settings. These get merged with the global settings.
 */
return array(
    // a MySQL driver configuration
    'default' => array(
        'type'		  => 'mysqli',
        'connection'  => array(
                'hostname'   => 'localhost',
                'username'   => 'root',
                'password'   => '',
                'port'       => '3306',
                'database'   => 'hugothunt',
                'compress'   => true,
        ),
        'profiling' => false
    )
);
