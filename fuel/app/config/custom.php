<?php
/*
 * 
 */

return array(
    /*
     * Upload path for photos
     */
    'upload_path'   => DOCROOT.DS.'uploads'.DS.date('Y-m-d').DS,
    'hugot_limit'   => 20,
);
