<?php

class Model_Core extends Orm\Model 
{
    protected static function social_profile($args)
    {
        $profile = array();
        if($args['social_network'] == 'twitter'){
            $profile['picture'] = 'https://twitter.com/'.$args['user_name'].'/profile_image?size=bigger';
            $profile['url']     = 'https://twitter.com'.$args['user_name'];    
        } else {
            $profile['picture'] = 'https://graph.facebook.com/'.$args['user_name'].'/picture/redirect=0&type=large';
            $profile['url']     = 'https://facebook.com/'.$args['user_name'];
        }
        return $profile;
    }
}
