<?php

class Model_Hugot_Comments extends Model_Core
{
    protected static $_properties = array(
        'id',
        'hugot_id',
        'user_id',
        'comment',
        'updated_at',
        'created_at'
    );
    
    protected static $_has_one  = array(
        'User_Index' => array(
            'key_from'      => 'user_id',
            'key_to'        => 'id',
            'model_to'      => 'Model_User_Index',
            'cascade_save'  => false,
            'cascade_delete'=> false,
        )
    );
    
    protected static $_belongs_to = array(
        'Hugot_Index' => array(
            'key_from'      => 'hugot_id',
            'key_to'        => 'id',
            'model_to'      => 'Model_Hugot_Index',
            'cascade_save'  => true,
            'cascade_delete'=> false
        ),
        'Hugot_Summary'=> array(
            'key_from'      => 'hugot_id',
            'key_to'        => 'id',
            'model_to'      => 'Model_Hugot_Summary',
            'cascade_save'  => false,
            'cascade_delete'=> false
        )
    );
    
    public static function add($args)
    {
        $q = new Model_Hugot_Comments();
        $q->hugot_id    = $args['hugot_id'];
        $q->user_id     = $args['user_id'];
        $q->comment     = $args['comment'];
        $q->updated_at  = date('Y-m-d');
        $q->save();
        
        $user = Model_User_Index::query()
                    ->where('id','=',$args['user_id'])
                    ->get_one();
        
        $args2            = array();
        $args2['user_id'] = $args['user_id'];
        $args2['key']     = 'name';
        
        $args['user_name'] = Model_User_Options::get_value($args2);
        
        $args2['social_network'] = $user['social_network'];
        $args2['user_name']      = ($args2['social_network'] == 'twitter')?
                                    $args['user_name']:$q['User_Index']['social_id'];
        $profile = self::social_profile($args2);
        
        $args['user_pic'] = $profile['picture'];
        
        return $args;
    }
    
    public static function edit($args)
    {
        $q = self::_general_query($args);
        if($q->count() == 1)
        {
            $row = $q->get_one();
            $row->comment = $args['comment'];
            return $row->save();
        }
    }
    
    public static function remove($args)
    {
        $q = self::_general_query($args);
        return $q->delete();
    }
    
    public static function count_comments($args)
    {
        $q = Model_Hugot_Comments::query()
                ->where('hugot_id','=',$args['hugot_id']);
        return $q->count();
    }


    private static function _general_query($args)
    {
        return Model_Hugot_Comments::query()
                    ->where('id','=',$args['comment_id'])
                    ->where('user_id','=',$args['user_id']);
    }
}
