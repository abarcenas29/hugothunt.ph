<?php

class Model_Hugot_Summary extends Model_Core
{
    protected static $_views = array(
        'hugot_summary' => array(
            'columns' => array(
                'id',
                'user_id',
                'photo_id',
                'hugot',
                'url',
                'comment_count',
                'upvotes',
                'created_at',
                'updated_at'
            )
        )
    );
    
    protected static $_has_many = array(
        'Hugot_Comments' => array(
            'key_from'      => 'id',
            'key_to'        => 'hugot_id',
            'model_to'      => 'Model_Hugot_Comments',
            'cascade_save'  => true,
            'cascade_delete'=> true
        ),
        'Hugot_Votes'   => array(
            'key_from'      => 'id',
            'key_to'        => 'hugot_id',
            'model_to'      => 'Model_Hugot_Votes',
            'cascade_save'  => true,
            'cascade_delete'=> true
        )
    );
    
    protected static $_has_one = array(
        'Photos'            => array(
            'key_from'      => 'photo_id',
            'key_to'        => 'id',
            'model_to'      => 'Model_Photos',
            'cascade_save'  => true,
            'cascade_delete'=> true
        ),
        'User_Index' => array(
            'key_from'      => 'user_id',
            'key_to'        => 'id',
            'model_to'      => 'Model_User_Index',
            'cascade_save'  => false,
            'cascade_delete'=> false,
        ),
    );
    
    protected static $_belongs_to = array(
        'Hugot_Index'   => array(
            'key_from'      => 'user_id',
            'key_to'        => 'id',
            'model_to'      => 'Model_User_Index',
            'cascade_save'  => true,
            'cascade_delete'=> false
        )
    );
    
    protected static $_table_name = 'hugot_summary';
    
    public static function loop($args)
    {
        $limit  = Config::get('custom.hugot_limit',20);
        $offset = $args['page'] * $limit;
        
        $q = Model_Hugot_Summary::query()
                ->order_by('updated_at','desc')
                ->order_by('upvotes','desc')
                ->order_by('hugot','desc')
                ->offset($offset)
                ->limit($limit);
        
        $result = array();
        $x      = 0;
        foreach($q->get() as $row){
            $args['user_id'] = $row['user_id'];
            $args['key']     = 'name';
            
            $result[$x]['user_name']  = Model_User_Options::get_value($args);
            $result[$x]['text']       = $row['hugot'];
            $result[$x]['source']     = $row['url'];
            
            $args['hugot_id']               = $row['id'];
            $result[$x]['upvotes']          = Model_Hugot_Votes::count_votes($args);
            $result[$x]['comments']         = Model_Hugot_Comments::count_comments($args);
            $result[$x]['voted']            = Model_Hugot_Votes::did_user_vote($args);
            $result[$x]['id']               = $row['id'];
            $result[$x]['user_id']          = $args['user_id'];
            
            $datetime            = new DateTime($row['created_at']);
            $date_format         = $datetime->format('F d, Y');
            
            $result[$x]['date']  = $date_format;
            
            if(!is_null($row['photo_id'])){
                $uri = 'uploads/'.$row['Photos']['date'].'/thumb-'.$row['Photos']['filename'];
                $result[$x]['meme'] = Uri::create($uri);
            }
            
            
            $my_collections         = array();
            $my_collection_query    = Model_Collection_Index::my_hugot_collection();
            $y                      = 0;
            foreach($my_collection_query as $collection){
                $args['collection_id']      = $collection['id'];
                
                if(!is_null($collection['photo_id'])){
                    $url = Uri::create('uploads/'.$collection['Photos']['date'].'/thumb-'.$collection['Photos']['filename']);
                    $my_collections[$y]['photo'] = $url;
                }
                
                $my_collections[$y]['added']= Model_Collection_Index::is_hugot_included($args);
                $my_collections[$y]['name'] = $collection['name'];
                $my_collections[$y]['id']   = $collection['id'];
                $y++;
            }
            $result[$x]['my_collections'] = $my_collections;
            
            $x++;
        }
        return $result;
    }
    
    public static function detail($args)
    {
        $q = Model_Hugot_Summary::query()
                ->where('id','=',$args['id']);
        
        $result = array();
        if($q->count() == 1){
            $row = $q->get_one();
            
            $args['hugot_id']= $row['id'];
            $args['user_id'] = $row['user_id'];
            $args['key']     = 'name';
            
            $result['user']     = Model_User_Options::get_value($args);
            $result['text']     = $row['hugot'];
            $result['source']   = $row['url'];
            $result['id']       = $row['id'];
            $result['user_id']  = $row['user_id'];
            
            $result['voted']    = Model_Hugot_Votes::did_user_vote($args);
            
            $datetime            = new DateTime($row['created_at']);
            $date_format         = $datetime->format('F d, Y');
            
            $result['date']  = $date_format;
            
            if(!is_null($row['photo_id'])){
                $uri = $row['Photos']['date'] .'/'.$row['Photos']['filename'];
                $result['meme'] = Uri::create('uploads/'.$uri);
            }
            
            $comments = array();
            $x        = 0;
            foreach($row['Hugot_Comments'] as $comment){
                $comment_args = array();
                $comment_args['user_id']        = $comment['user_id'];
                $comment_args['key']            = 'name';
                $comment_args['social_network'] = $comment['User_Index']['social_network'];
                $comment_args['user_name']      = ($comment_args['social_network'] == 'twitter')?
                                                    Model_User_Options::get_value($comment_args):
                                                    $comment['User_Index']['social_id'];
                
                $profile = self::social_profile($comment_args);
                
                $comments[$x]['id']         = $comment['user_id'];
                $comments[$x]['name']       = Model_User_Options::get_value($comment_args);
                $comments[$x]['pic']        = $profile['picture'];
                $comments[$x]['comment']    = $comment['comment'];
                $x++;
            }
            $result['comments'] = $comments;
            
            $votes = array();
            $x     = 0;
            $limit = 5;
            foreach($row['Hugot_Votes'] as $vote){
                //if($x > $limit)break;
                $vote_args['user_id']       = $vote['user_id'];
                $vote_args['key']           = 'name';
                $vote_args['social_network']= $vote['User_Index']['social_network'];
                $vote_args['user_name']     = ($vote_args['social_network'] == 'twitter')?
                                                Model_User_Options::get_value($vote_args):
                                                $vote['User_Index']['social_id'];
                
                $profile                    = self::social_profile($vote_args);
                
                $votes[$x]['pic']       = $profile['picture'];
                $votes[$x]['user_name'] = Model_User_Options::get_value($vote_args);
                $x++;
            }
            $result['upvoters'] = $votes;
            
            $collections = array();
            $x           = 0;
            $limit       = 5;
            $args['hugot_id'] = $result['id'];
            $collection_query = Model_Collection_Index::hugot_collection($args);
            foreach($collection_query as $collection){
                //if($x > 5)break;
                $collections[$x]['name'] = $collection['name'];
                $collections[$x]['id']   = $collection['id'];
                
                $url = Uri::create('uploads/'.$collection['Photos']['date'].'/thumb-'.$collection['Photos']['filename']);
                $collections[$x]['photo']= $url;
                $x++;
            }
            $result['collections']      = $collections;
            $result['commentcount']     = Model_Hugot_Comments::count_comments($args);
            $result['upvotes']          = Model_Hugot_Votes::count_votes($args);
            
            return $result;
        }
    }
}
