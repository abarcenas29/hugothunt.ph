<?php

class Model_Hugot_Index extends Model_Core
{
    protected static $_properties = array(
        'id',
        'user_id',
        'photo_id',
        'hugot',
        'url',
        'updated_at',
        'created_at'
    );
    
    protected static $_has_many = array(
        'Hugot_Comments' => array(
            'key_from'      => 'id',
            'key_to'        => 'hugot_id',
            'model_to'      => 'Model_Hugot_Comments',
            'cascade_save'  => true,
            'cascade_delete'=> true,
            'condition'     => array(
                'ordery_by' => array(
                    'id' => 'DESC'
                )
            )
        ),
        'Hugot_Votes'   => array(
            'key_from'      => 'id',
            'key_to'        => 'hugot_id',
            'model_to'      => 'Model_Hugot_Votes',
            'cascade_save'  => true,
            'cascade_delete'=> true
        )
    );
    
    protected static $_has_one   = array(
        'Photos'            => array(
            'key_from'      => 'photo_id',
            'key_to'        => 'id',
            'model_to'      => 'Model_Photos',
            'cascade_save'  => true,
            'cascade_delete'=> true
        ),
    );
    
    protected static $_belongs_to = array(
        'Hugot_Index'   => array(
            'key_from'      => 'user_id',
            'key_to'        => 'id',
            'model_to'      => 'Model_User_Index',
            'cascade_save'  => true,
            'cascade_delete'=> false
        )
    );
    
    protected static $_table_name = 'hugot_index';

    public static function add($args)
    {
        $q = new Model_Hugot_Index();
        $q->user_id = $args['user_id'];
        
        $args = Model_Photos::add($args);
        
        if(!is_null($args['photos']))
        {
            foreach($args['photos'] as $photo)
            {
                $q->photo_id= $photo['id'];
            }
        } else {
            $q->photo_id = null;
        }
        
       
        $q->updated_at  = date('Y-m-d');
        $q->hugot       = $args['hugot'];
        $q->url         = $args['url'];
        $q->save();
        return $q->id;
    }
    
    public static function edit($args)
    {
        $q = Model_Hugot_Index::query()
                ->where('id','=',$args['hugot_id'])
                ->get_one();
        $q->hugot   = $args['hugot'];
        $q->user_id = $args['user_id'];
        $q->url     = $args['url'];
        $q->updated_at  = date('Y-m-d');
        
        if(count($_FILES) > 0){
            if(!is_null($q['photo_id'])){
                $args['photo_id'] = $q['photo_id'];
                Model_Photos::edit($args);
            } else {
                $args = Model_Photos::add($args);
                if(!is_null($args['photos']))
                {
                    foreach($args['photos'] as $photo)
                    {
                        $q->photo_id= $photo['id'];
                    }
                } else {
                    $q->photo_id = null;
                }
            }
        }
        
        return $q->save();
    }


    public static function remove($args)
    {
        $q = Model_Hugot_Index::find($args['hugot_id'],array(
            'related' => array(
                'Hugot_Comments',
                'Hugot_Votes'
            )));
        $args['photo_id'] = $q['photo_id'];
        
        Model_Photos::remove($args);
        Model_Collection_Hugots::delete_hugots($args);
        
        return $q->delete();
    }
    
    /*
     * Used in user detail
     */
    public static function hugot_loop_form($query)
    {
        $hugots     = array();
        $hugot_args = array();
        $x      = 0;
        foreach($query as $hugot){
            $hugots[$x]['text'] = $hugot['hugot'];
            $hugots[$x]['id']   = $hugot['id'];
            
            if(!is_null($hugot['photo_id'])){
                $url_photos = Uri::create('uploads/'.$hugot['Photos']['date'].'/small-'.$hugot['Photos']['filename']);
                $hugots[$x]['photo'] = $url_photos;
            }
            
            $hugot_args['hugot_id']     = $hugot['id'];
            $hugots[$x]['upvotes']      = Model_Hugot_Votes::count_votes($hugot_args);
            $hugots[$x]['comments']     = Model_Hugot_Comments::count_comments($hugot_args);
            $x++;
        }
        return $hugots;
    }
}
