<?php

class Model_Hugot_Votes extends Model_Core 
{
    protected static $_properties = array(
        'id',
        'hugot_id',
        'user_id',
        'created_at'
    );
    
    protected static $_belongs_to = array(
        'Hugot_Index' => array(
            'key_from'          => 'hugot_id',
            'key_to'            => 'id',
            'model_to'          => 'Model_Hugot_Index',
            'cascading_save'    => true,
            'cascading_delete'  => false,
        ),
        'Hugot_Summary' => array(
            'key_from'          => 'hugot_id',
            'key_to'            => 'id',
            'model_to'          => 'Model_Hugot_Index',
            'cascading_save'    => false,
            'cascading_delete'  => false 
        )
    );
    
    protected static $_has_one = array(
        'User_Index'            => array(
            'key_from'          => 'user_id',
            'key_to'            => 'id',
            'model_to'          => 'Model_User_Index',
            'cascading_save'    => false,
            'cascading_delete'  => false
        )
    );
    
    public static function vote($args)
    {
        $q = Model_Hugot_Votes::query()
                ->where('user_id','=',$args['user_id'])
                ->where('hugot_id','=',$args['hugot_id']);
        $args['status'] = "error";
        if($q->count() > 0){
            $q->delete();
            $args['status'] = "unvote";
            return $args;
        } else {
            $q = new Model_Hugot_Votes();
            $q->hugot_id    = $args['hugot_id'];
            $q->user_id     = $args['user_id'];
            $q->save();
            
            $args['status'] = "voted";
            return $args;
        }
        $q = Model_Hugot_Votes::query()
                ->where('hugot_id','=',$args['hugot_id']);
        $args['votes']  = $q->count();
        return $args;
    }
    
    public static function count_votes($args)
    {
        $q = Model_Hugot_Votes::query()
                ->where('hugot_id','=',$args['hugot_id']);
        return $q->count();
    }
    
    public static function did_user_vote($args)
    {
        $q = Model_Hugot_Votes::query()
                ->where('hugot_id','=',$args['hugot_id'])
                ->where('user_id','=',$args['user_id']);
        return ($q->count() == 0)?false:true;
    }
}
