<?php

class Model_Photos extends Model_Core
{
    protected static $_properties = array(
        'id',
        'date',
        'filename',
        'created_at'
    );
    
    protected static $_belongs_to = array(
        'Collection_Index' => array(
            'key_from'          => 'id',
            'key_to'            => 'photo_id',
            'model_to'          => 'Model_Collection_Index',
            'cascading_save'    => true,
            'cascading_delete'  => false
        ),
        'Hugot_Index_Photos' => array(
            'key_from'          => 'id',
            'key_to'            => 'photo_id',
            'model_to'          => 'Model_Hugot_Index',
            'cascading_save'    => true,
            'cascading_delete'  => false
        )
    );
    
    public static function add($args)
    {
        try {
            if(Upload::is_valid()){
                $args['upload_path']   = Config::get('custom.upload_path');
                $args['date']          = date('Y-m-d');

                Upload::save($args['upload_path']);
                $args = Model_Photos::_process_upload($args);

                $x = 0;
                foreach($args['photos'] as $photo){
                    $q          = new Model_Photos();
                    $q->date    = date('Y-m-d');
                    $q->filename= $photo['saved_as'];
                    $q->save();

                    $args['photos'][$x]['id'] = $q->id;
                }

                return $args;
            }
        } catch (Exception $ex) {
            $args['photos'] = null;
            return $args;
        }
    }
    
    public static function edit($args)
    {
        if(Upload::is_valid()){
            $args['upload_path'] = Config::get('custom.upload_path');
            $args['date']        = date('Y-m-d');
            
            Upload::save($args['upload_path']);
            $args = Model_Photos::_process_upload($args);
            
            $x = 0;
            foreach($args['photos'] as $photo){
                $q  = Model_Photos::query()
                        ->where('id','=',$args['photo_id'])
                        ->get_one();
                
                Model_Photos::remove_file($q);
                
                $q->date        = $args['date'];
                $q->filename    = $photo['saved_as'];
                $q->save();
                
                return true;
            }
        }
    }
    
    public static function remove_file($q)
    {
        $path = DOCROOT.DS.'uploads'.DS.$q['date'].DS;
        
        $files = array('','small-','thumb-');
        foreach($files as $file)
        {
            $filepath = $path.$file.$q['filename'];
            if(file_exists($filepath)){
                File::delete($filepath);
            }
        }
    }


    public static function remove($args)
    {
        $q = Model_Photos::query()
                ->where('id','=',$args['photo_id'])
                ->get_one();
        $path = DOCROOT.DS.'uploads'.DS.$q['date'].DS;
        
        $files = array('','small-','thumb-');
        
        foreach($files as $file)
        {
            $filepath = $path.$file.$q['filename'];
            if(file_exists($filepath)){
                File::delete($filepath);
            }
        }
        
        return $q->delete();
    }
    
    private static function _process_upload($args)
    {
        /*
         * 1. convert pngs to jpeg
         * 2. resize all of them
         * 3. pass the new arguments back
         */
        
        $photos                = Upload::get_files();
        
        //Place to store the processed filenames
        $args['photos']        = array();
        
        foreach($photos as $photo)
        {
            $photo = Model_Photos::_convert_to_jpg($photo);
            Model_Photos::_resize_img($photo);
            $args['photos'][] = $photo;
        }
        return $args;
    }
    
    private static function _resize_img($photo)
    {
        $imageFile = $photo['saved_to'].DS.$photo['saved_as'];
        
        //shrink orig file
        Image::load($imageFile)
            ->resize(1024,null,true)
            ->save_pa('','');
        
        //shrink medium
        Image::load($imageFile)
            ->resize(640,null,true)
            ->save_pa('small-');
        
        //make thumb
        Image::load($imageFile)
            ->crop_resize(290,290)
            ->save_pa('thumb-');
    }
    
    private static function _convert_to_jpg($photo)
    {
        /*
         * If its not a PNG file, it will just be bypassed
         */
        if($photo['mimetype'] === 'image/png'){
            $imageFile  = $photo['saved_to'].DS.$photo['saved_as'];
            
            //generate random file for new image name
            $rand_filename = strtoupper(substr_replace(sha1(microtime(true)),'',25)).'.jpg';
            
            //convert the file
            $imgJpg     = imagecreatefrompng($imageFile);
            
            //create a fille for the transparent sections of the image
            $fillImage  = imagecreatetruecolor(imagesx($imgJpg),imagesy($imgJpg));
            imagealphablending($fillImage,true);
            //blend then altogether
            imagecopy($fillImage,$imgJpg,0,0,0,0,imagesx($imgJpg),imagesy($imgJpg));
            //destroy base image
            imagedestroy($imgJpg);
            
            //save final image
            imagejpeg($fillImage,$photo['saved_to'].DS.$rand_filename,75);
            
            //delete original image
            File::delete($imageFile);
            
            //overwrite to $photo
            $photo['saved_as'] = $rand_filename;
            
            //return the value
            return $photo;
        }
        return $photo;
    }
}
