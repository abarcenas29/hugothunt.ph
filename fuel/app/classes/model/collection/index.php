<?php

class Model_Collection_Index extends Model_Core
{
    protected static $_properties = array(
        'id',
        'user_id',
        'photo_id',
        'name',
        'description',
        'created_at'
    );
    
    protected static $_has_many = array(
        'Collection_Lists' => array(
            'key_from'      => 'id',
            'key_to'        => 'collection_id',
            'model_to'      => 'Model_Collection_Hugots',
            'cascade_save'  => true,
            'cascade_delete'=> true,
            'condition'    => array(
                'order_by' => array(
                    'Hugot_Index.updated_at' => 'DESC'
                    )
            )
        )
    );
    
    protected static $_has_one = array(
        'User_Index'        => array(
            'key_from'      => 'user_id',
            'key_to'        => 'id',
            'model_to'      => 'Model_User_Index',
            'cascade_save'  => true,
            'cascade_delete'=> false
        ),
        'Photos'            => array(
            'key_from'      => 'photo_id',
            'key_to'        => 'id',
            'model_to'      => 'Model_Photos',
            'cascade_save'  => true,
            'cascade_delete'=> true
        )
    );
    
    protected static $_table_name = 'collection_index';
    
    //get the collection based on a hugot line
    public static function hugot_collection($args)
    {
        return Model_Collection_Index::query()
                ->related('Collection_Lists')
                ->where('Collection_Lists.hugot_id','=',$args['hugot_id'])
                ->get();
    }
    
    public static function my_hugot_collection()
    {
        $user_id = Session::get('user_id',0);
        return Model_Collection_Index::query()
                    ->where('user_id','=',$user_id)
                    ->get();
    }
    
    public static function is_hugot_included($args)
    {
        $q = Model_Collection_Index::query()
                ->related('Collection_Lists')
                ->where('id','=',$args['collection_id'])
                ->where('Collection_Lists.hugot_id','=',$args['hugot_id']);
        return ($q->count() > 0)?true:false;
    }
    
    public static function detail($args)
    {
        $result = array();
        $q = Model_Collection_Index::query()
                ->where('id','=',$args['collection_id']);

        if($q->count() > 0){
            $row = $q->get_one();
            $result['collection_name'] = $row['name'];
            $result['description']     = $row['description'];
            $result['collection_id']   = $row['id'];
            
            if(is_null($row['photo_id'])){
                $url = 'uploads/'.$row['Photos']['date'].'/thumb-'.$row['Photos']['filename'];
                $result['collection_photo'] = Uri::create($url);
            }
            
            $result['hugots'] = array();
            $x                = 0;
            foreach($row['Collection_Lists'] as $hugot){
                $key    = 'hugots';
                $index  = 'Hugot_Index';
                
                $args2['user_id'] = $hugot[$index]['user_id'];
                $args2['key']     = 'name';
                $args2['hugot_id']= $hugot['hugot_id'];
                
                
                $result[$key][$x]['id']        = $hugot['hugot_id'];
                $result[$key][$x]['user_id']   = $args2['user_id'];
                $result[$key][$x]['user_name'] = Model_User_Options::get_value($args2);
                $result[$key][$x]['voted']     = Model_Hugot_Votes::did_user_vote($args2);
                $result[$key][$x]['text']      = $hugot['Hugot_Index']['hugot'];
                $result[$key][$x]['source']    = $hugot['Hugot_Index']['url'];
                
                $args2['hugot_id'] = $hugot['hugot_id'];
                $result[$key][$x]['upvotes']   = Model_Hugot_Votes::count_votes($args2);
                $result[$key][$x]['comments']  = Model_Hugot_Comments::count_comments($args2);
                
                $datetime = new DateTime($hugot[$index]['created_at']);
                $format   = $datetime->format('F d, Y');
                
                $result[$key][$x]['date'] = $format;
                
                if(!is_null($row['photo_id'])){
                    $uri = 'uploads/'.$hugot[$index]['Photos']['date'].'/thumb-'.$hugot[$index]['Photos']['filename'];
                    $result[$key][$x]['meme'] = Uri::create($uri);
                }
                
                $x++;
            }
            
            return $result;
        }
    }
    
    public static function loop($args)
    {
        $limit = Config::get('custom.hugot',200);
        $offset= $args['page'] * $limit;
        
        $q = Model_Collection_Index::query()
                ->order_by('created_at','desc')
                ->offset($offset)
                ->limit($limit);
        
        $results = array();
        $x       = 0;
        foreach($q->get() as $row){
            $results[$x]['id'] = $row['id'];
            
            if(!is_null($row['photo_id'])){
                $url = '/uploads/'.$row['Photos']['date'].'/thumb-'.$row['Photos']['filename'];
                $results[$x]['meme'] = Uri::create($url);
            }
            
            $datetime   = new DateTime($row['created_at']);
            $format     = $datetime->format('F d, Y');
            $results[$x]['date'] = $format;
            
            $results[$x]['title']       = $row['name'];
            $results[$x]['description'] = $row['description'];
            $results[$x]['user_id']     = $row['user_id'];
            
            $user_args = array();
            $user_args['user_id']       = $row['user_id'];
            $user_args['key']           = 'name';
            $user_args['social_network']= $row['User_Index']['social_network'];
            $user_args['user_name']     = ($user_args['social_network'] == 'twitter')?
                                                Model_User_Options::get_value($user_args):
                                                $row['User_Index']['social_id'];
            $user_args['collection_id'] = $row['id'];
            
            $profile = self::social_profile($user_args);
            $results[$x]['user_pic'] = $profile['picture'];
            $results[$x]['user']     = Model_User_Options::get_value($user_args);
            $results[$x]['hugots']   = Model_Collection_Hugots::count_hugots($user_args);
            $x++;
        }
        return $results;
    }


    public static function add($args)
    {
        $q = new Model_Collection_Index();
        $q->user_id     = $args['user_id'];
        
        $args           = Model_Photos::add($args);
        
        if(!is_null($args['photos'])){
            $q->photo_id    = $args['photos'][0]['id'];
        } else {
            $q->photo_id    = null;
        }
        
        $q->name        = $args['name'];
        $q->description = $args['description'];
        $q->save();
        
        $args['collection_id'] = $q->id;
        return $args;
    }
    
    public static function edit($args)
    {
        $q = Model_Collection_Index::query()
                ->where('user_id','=',$args['user_id'])
                ->where('id','=',$args['collection_id']);
        if($q->count() == 1){
            $row        = $q->get_one();
            $row->name  = $args['name'];
            
            if(is_null($row['photo_id']) && count($_FILES) > 0){
                $args = Model_Photos::add($args);
                $row->photo_id = $args['photos'][0]['id'];
            } else if (count($_FILES) > 0) {
                $args['photo_id'] = $row['photo_id'];
                Model_Photos::edit($args);
            }
            
            $row->save();
            return true;
        }
        return false;
    }
    
    public static function remove($args)
    {
        $q = Model_Collection_Index::find($args['collection_id'],array(
            'related' => array(
                'Collection_Lists'
            )
        ));
        $args['photo_id']   =   $q['photo_id'];
        
        Model_Photos::remove($args);
        return $q->delete();
    }
}
