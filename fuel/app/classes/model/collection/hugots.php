<?php

class Model_Collection_Hugots extends Model_Core
{
    protected static $_properties = array(
        'id',
        'collection_id',
        'hugot_id',
        'created_at'
    );
    
    
    protected static $_has_one = array(
        'Hugot_Index' => array(
            'key_from'      => 'hugot_id',
            'key_to'        => 'id',
            'model_to'      => 'Model_Hugot_Index',
            'cascade_save'  => false,
            'cascade_delete'=> false
        )
    );
    
    protected static $_belongs_to = array(
        'Collection_Index'  => array(
            'key_from'      => 'collection_id',
            'key_to'        => 'id',
            'model_to'      => 'Model_Collection_Index',
            'cascade_save'  => false,
            'cascade_delete'=> false,
        )
    );


    public static function delete_hugots($args)
    {
        return Model_Collection_Hugots::query()
                    ->where('hugot_id','=',$args['hugot_id'])
                    ->delete();
    }
    
    public static function add($args)
    {
        $hugot = Model_Hugot_Index::query()
                    ->where('id','=',$args['hugot_id']);
        if($hugot->count() == 1){
            $q = new Model_Collection_Hugots();
            $q->collection_id = $args['collection_id'];
            $q->hugot_id      = $args['hugot_id'];
            $q->save();
            $args['huggot_collection_id'] = $q->id;
            return $args;
        }
        $args['error'] = "Hugot Id not exsist";
        return $args;
    }
    
    public static function remove($args)
    {
        $q = Model_Collection_Index::query()
                ->where('user_id','=',$args['user_id'])
                ->where('Collection_Lists.id','=',$args['huggot_collection_id']);
        if($q->count() == 1){
            $hugot = Model_Collection_Hugots::query()
                        ->where('id','=',$args['huggot_collection_id']);
            $hugot->delete();
            return true;
        }
        return false;
    }
    
    public static function count_hugots($args)
    {
        return Model_Collection_Hugots::query()
                    ->where('collection_id','=',$args['collection_id'])
                    ->count();
    }
}
