<?php

class Model_User_Index extends Model_Core
{
    protected static $_properties = array(
        'id',
        'social_id',
        'social_network',
        'email',
        'created_at'
    );
    
    protected static $_has_many = array(
        'User_Options' => array(
            'key_from'          => 'id',
            'key_to'            => 'user_id',
            'model_to'          => 'Model_User_Options',
            'cascade_save'      => true,
            'cascade_delete'    => true
        ),
        'User_Follows' => array(
            'key_from'          => 'id',
            'key_to'            => 'user_id',
            'model_to'          => 'Model_User_Follows',
            'cascade_save'      => true,
            'cascade_delete'    => true
        ),
        'Hugot_Index' => array(
            'key_from'          => 'id',
            'key_to'            => 'user_id',
            'model_to'          => 'Model_Hugot_Index',
            'cascade_save'      => true,
            'cascade_delete'    => true
        )
    );
    
    protected static $_belongs_to = array(
        'User_Index'        => array(
            'key_from'      => 'id',
            'key_to'        => 'user_id',
            'model_to'      => 'Model_User_Follows',
            'cascade_save'  => false,
            'cascade_delete'=> false,
        ),
        'User_Follow'       => array(
            'key_from'      => 'id',
            'key_to'        => 'user_follow',
            'model_to'      => 'Model_User_Follows',
            'cascade_save'  => false,
            'cascade_delete'=> false
        ),
        'Hugot_Votes_Users' => array(
            'key_from'      => 'id',
            'key_to'        => 'user_id',
            'model_to'      => 'Model_Hugot_Votes',
            'cascade_save'  => false,
            'cascade_delete'=> false
        ),
        'User_Index_Collection' => array(
            'key_from'      => 'id',
            'key_to'        => 'user_id',
            'model_to'      => 'Model_Collection_Index',
            'cascade_save'  => false,
            'cascade_delete'=> false
        ),
        'Hugot_User_Summary' => array(
            'key_from'      => 'id',
            'key_to'        => 'user_id',
            'model_to'      => 'Model_Hugot_Summary',
            'cascade_save'  => false,
            'cascade_delete'=> false
        )
    );
    
    protected static $_table_name = 'user_index';
    
    public static function detail($args)
    {
        $result = array();
        $user_query = Model_User_Index::query()
                        ->where('id','=',$args['user_id']);
        
        $args['key']            = 'name';
        $result['username']     = Model_User_Options::get_value($args);
        
        //no user found
        if($result === false){
            return $result;
        }
        $row = $user_query->get_one();
        
        $datetime = new DateTime($row['created_at']);
        $result['joined'] = $datetime->format('F d, Y');
        
        //determine if facebook or twitter
        $args['social_network'] = $row['social_network'];
        $args['user_name']      = ($row['social_network'] == 'twitter')?
                                    $result['user_name']:
                                    $row['social_id'];
        $profile                = self::social_profile($args);
        
        $result['id']               = $row['id'];
        $result['social_url']       = $profile['url'];
        $result['social_photo']     = $profile['picture'];
        $result['social_network']   = $args['social_network'];
        $result['email']            = $row['email'];
        
        //If You are following the user
        $args['signed_user'] = Session::get('user_id',null);
        if(!is_null($args['signed_user']) && $args['signed_user'] !== $row['id']){
            $followed_query = Model_User_Index::query()
                                ->related('User_Follows')
                                ->where('User_Follows.user_id','=',$args['signed_user'])
                                ->where('User_Follows.user_follow','=',$row['id']);
            $result['is_followed'] = ($followed_query->count() > 0)?true:false;
        }
        
        
        //hugot submissions
        $hugot_query = Model_Hugot_Summary::query()
                            ->where('user_id','=',$args['user_id'])
                            ->order_by('updated_at','DESC')
                            ->order_by('upvotes','DESC')
                            ->order_by('hugot','DESC')
                            ->get();
        
        $result['submissions'] = Model_Hugot_Index::hugot_loop_form($hugot_query);
        
        //hugots you voted
        $hugot_query = Model_Hugot_Index::query()
                            ->related('Hugot_Votes')
                            ->where('Hugot_Votes.user_id','=',$args['user_id'])
                            ->get();
        $result['upvoted'] = Model_Hugot_Index::hugot_loop_form($hugot_query);
        
        //collection you made
        $collection_query = Model_Collection_Index::query()
                                ->where('user_id','=',$args['user_id'])
                                ->get();
        $collections    = array();
        $x              = 0;
        $collection_args= array();
        foreach($collection_query as $collection){
            $collections[$x]['title'] = $collection['name'];
            if(!is_null($collection['photo_id'])){
                $url_photos = Uri::create('uploads/'.$collection['Photos']['date'].'/small-'.$collection['Photos']['filename']);
                $collections[$x]['cover'] = $url_photos;
            }
            $collection_args['collection_id']      = $collection['id'];
            $collections[$x]['id']                 = $collection['id'];
            $collections[$x]['hugots']             = Model_Collection_Hugots::count_hugots($collection_args);
            $x++;
        }
        $result['collections'] = $collections;
        
        //users you follow
        $followings = array();
        $x          = 0;
        $following_args = array();
        foreach($row['User_Follows'] as $following){
            $following_args['user_id']          = $following['user_follow'];
            $following_args['key']              = 'name';
            $following_args['social_network']   = $following['User_Follow']['social_network'];
            $following_args['user_name']        = ($following_args['social_network'] == 'twitter')?
                                                    Model_User_Options::get_value($following_args):
                                                    $following['User_Follow']['social_id'];
            
            $profile = self::social_profile($following_args);
            
            $followings[$x]['id']   = $following_args['user_id'];
            $followings[$x]['name'] = Model_User_Options::get_value($following_args);
            $followings[$x]['url']  = $profile['url'];
            $followings[$x]['pic']  = $profile['picture'];
            
            $x++;
        }
        $result['following'] = $followings;
        
        //pepople the follows you
        $followed_query = Model_User_Follows::query()
                            ->where('user_follow','=',$args['user_id'])
                            ->get();
        $followeds      = array();
        $followed_args  = array();
        $x              = 0;
        foreach($followed_query as $followed)
        {
            $followed_args['user_id']           = $followed['user_id'];
            $followed_args['key']               = 'name';
            $followed_args['social_network']    = $followed['User_Index']['social_network'];
            $followed_args['user_name']         = ($followed_args['social_network'] == 'twitter')?
                                                    Model_User_Options::get_value($followed_args):
                                                    $followed['User_Index']['social_id'];
            
            $profile = self::social_profile($followed_args);
            
            $followeds[$x]['id']        = $followed_args['user_id'];
            $followeds[$x]['name']      = Model_User_Options::get_value($followed_args);
            $followeds[$x]['url']       = $profile['url'];
            $followeds[$x]['pic']       = $profile['picture'];
            $x++;
        }
        $result['followers'] = $followeds;
        
        return $result;
    }

    public static function authenticate($args)
    {
        $q = Model_User_Index::query()
                ->where('social_network','=',$args['social_network'])
                ->where('social_id','=',$args['social_id']);
        if($q->count() == 1){
            $row        = $q->get_one();
            $row->email = $args['email'];
            
            $args['id']     = $row['id'];
            $args['value']  = $args['name'];
            $args['key']    = 'name';
            Model_User_Options::change_option($args);
            
            Model_User_Index::write_sessions($args);
            return $args;
        } else {
            $reg                         = new Model_User_Index();
            $reg->social_id              = $args['social_id'];
            $reg->social_network         = $args['social_network'];
            $reg->email                  = $args['email'];
            $reg->User_Options[0]        = new Model_User_Options();
            $reg->User_Options[0]->key   = 'name';
            $reg->User_Options[0]->value = $args['name'];
            $reg->save();
            
            $args['id'] = $reg->id;
            
            Model_User_Index::write_sessions($args);
            return $args;
        }
        
        return false;
    }
    
    private static function write_sessions($args)
    {
        Session::set('user_id',$args['id']);
        Session::set('social_id',$args['social_id']);
        Session::set('email',$args['email']);
        Session::set('name',$args['name']);
        Session::set('social_network',$args['social_network']);
    }


    public static function logout()
    {
        Session::delete('user_id');
        Session::delete('social_id');
        Session::delete('email');
        Session::delete('name');
        Session::delete('social_network');
        return true;
    }
}
