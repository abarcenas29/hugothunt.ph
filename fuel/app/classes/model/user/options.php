<?php

class Model_User_Options extends Model_Core
{
    protected static $_properties = array(
        'id',
        'user_id',
        'key',
        'value',
        'created_at'
    );
    
    protected static $_belongs_to = array(
        'User_Index' => array(
            'key_from'          => 'user_id',
            'key_to'            => 'id',
            'model_to'          => 'Model_User_Index',
            'cascade_save'      => false,
            'cascade_delete'    => false
        ),
    );

    public static function change_option($args)
    {
        $q = Model_User_Options::query()
                ->where('user_id','=',$args['id'])
                ->where('key','=',$args['key']);
        if($q->count() == 1){
            $row        = $q->get_one();
            $row->value = $args['value'];
            return $row->save();
        }
        return false;
    }
    
    public static function get_value($args)
    {
        $q = Model_User_Options::query()
                ->where('user_id','=',$args['user_id'])
                ->where('key','=',$args['key']);
        if($q->count() == 1){
            $row = $q->get_one();
            return $row['value'];
        }
        return false;
    }
}
