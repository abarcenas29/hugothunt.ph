<?php

class Model_User_Follows extends Model_Core
{
    protected static $_properties = array(
        'id',
        'user_id',
        'user_follow',
        'created_at'
    );
    
    protected static $_has_one    = array(
        'User_Index'    => array(
            'key_from'      => 'user_id',
            'key_to'        => 'id',
            'model_to'      => 'Model_User_Index',
            'cascade_save'  => false,
            'cascade_delete'=> false
        ),
        'User_Follow'   => array(
            'key_from'      => 'user_follow',
            'key_to'        => 'id',
            'model_to'      => 'Model_User_Index',
            'cascade_save'  => false,
            'cascade_delete'=> false
        )
    );
    
    protected static $_belongs_to = array(
        'User_Index' => array(
            'key_from'          => 'user_id',
            'key_to'            => 'id',
            'model_to'          => 'Model_User_Index',
            'cascade_save'      => false,
            'cascade_delete'    => false
        ),
    );


    public static function index()
    {
        $user_id = Session::get('user_id');
        $q = Model_User_Follows::query()
                ->where('user_id','=',$user_id)
                ->get();
        
        $arga    = array();
        $follows = array();
        $x       = 0;
        foreach($q as $row)
        {
            $args['user_id']     = $row['id'];
            $args['key']         = 'name';
            
            $follows[$x]['name'] = Model_User_Options::get_value($args);
            
            //determine if facebook or twitter
            if($row['User_Follow']['social_network'] == 'facebook'){
                $url    = 'http://facebook.com/'.$row['User_Follow']['social_id'];
                $photo  = 'https://graph.facebook.com/'
                            .$row['User_Follow']['social_id']
                            .'/picture/redirect=0&type=large';
            } else {
                $url    = 'http://twitter.com/'.$follows[$x]['name'];
                $photo  = 'https://twitter.com/'
                            .$follows[$x]['name']['user_name']
                            .'/profile_image?size=bigger';
            }
            
            $follows[$x]['photo']           = $photo;
            $follows[$x]['social_profile']  = $url;
            
            $follows[$x]['id']   = $row['id'];
            $x++;
        }
        
        return $follows;
    }
    
    public static function add($args)
    {
        $rsp = array();
        $user_id = Session::get('user_id');
        $q = Model_User_Follows::query()
                ->where('user_id','=',$user_id)
                ->where('user_follow','=',$args['user_follow']);
        if($q->count() == 0)
        {
            $q  = new Model_User_Follows();
            $q->user_id         = $user_id;
            $q->user_follow     = $args['user_follow'];
            $q->save();
            $rsp['status'] = 'Ok';
            $rsp['user_id']= $q->id;
            return $rsp;
        }
       
        $q->delete();
        $rsp['status'] = 'unfollowed';
        $rsp['user_id']= $args['user_follow'];
        return $rsp;
    }
}
