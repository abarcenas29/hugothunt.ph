<?php

class Controller_Collections extends Controller_Core
{
    public function action_add()
    {
        $view = View::forge('collections/add');
        $this->template->content = $view;
    }
    
    public function action_list()
    {
        $view = View::forge('collections/list');
        $this->template->content = $view;
    }
    
    public function action_index($id = null)
    {
        $view = View::forge('collections/wo_id');
        $this->template->content = $view;
    }
    
    public function action_detail($id = null)
    {
        $view                    = View::forge('collections/with_id');
        $view->id                = $id;
        $this->template->content = $view;
    }
}
