<?php

class Controller_User extends Controller_Core
{
    public function action_detail($id)
    {
        $view = View::forge('user');
        $view->id = $id;
        
        $this->template->content = $view;
    }
}
