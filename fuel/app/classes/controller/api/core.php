<?php
/*
 * Main Controller for API Calls
 * 
 * See http://fuelphp.com/docs/general/controllers/rest.html
 */
class Controller_Api_Core extends Controller_Rest 
{
    protected function _is_logged()
    {
        $user_id = Session::get('user_id',null);
        if(!is_null($user_id)){
            return true;
        }
        return false;
    }
    
    protected function _is_null($args)
    {
        foreach($args as $row)
        {
            if(is_null($row)){
                return true;
            }
        }
        return false;
    }
}
