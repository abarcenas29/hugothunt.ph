<?php

class Controller_Api_Collection extends Controller_Api_Core
{
    public function post_loop()
    {
        $args           = array();
        $args['page']   = Input::post('page',0);
        
        $result = Model_Collection_Index::loop($args);
        return $this->response($result,200);
    }

    public function post_detail()
    {
        $args                   = array();
        $args['collection_id']  = Input::post('collection_id');
        
        $result = Model_Collection_Index::detail($args);
        return $this->response($result,200);
    }

    public function post_add()
    {
        $args = array();
        $args['user_id']        = Session::get('user_id',null);
        $args['name']           = Input::post('name','');
        $args['description']    = Input::post('description','');
        
        $is_auth = $this->_is_logged();
        if(!empty($args['name']) && $is_auth){
            $args = Model_Collection_Index::add($args);
            return $this->response($args,200);
        }
        return $this->response($args,403);
    }
    
    public function post_edit()
    {
        $args                   = array();
        $args['user_id']        = Session::get('user_id',null);
        $args['collection_id']  = Input::post('collection_id',null);
        $args['name']           = Input::post('name','');
        
        $is_auth = $this->_is_logged();
        $is_null = $this->_is_null($args);
        if(!is_null($args['name']) && 
           $is_auth                && 
           !is_null($is_null)){
            if(Model_Collection_Index::edit($args)){
                return $this->response($args,200);
            }
        }
        return $this->response($args,403);
    }
    
    public function post_remove()
    {
        $args                   = array();
        $args['collection_id']  = Input::post('collection_id',null);
        $args['user_id']        = Session::get('user_id',null);
        
        $is_auth = $this->_is_logged();
        if($is_auth && !is_null($args['collection_id'])){
            Model_Collection_Index::remove($args);
            return $this->response($args,200);
        }
        return $this->response($args,403);
    }
    
    public function post_add_hugot()
    {
        $args                   = array();
        $args['hugot_id']       = Input::post('hugot_id',null);
        $args['collection_id']  = Input::post('collection_id',null);
        $args['user_id']        = Session::get('user_id',null);
        
        $is_auth = $this->_is_logged();
        if($is_auth && !is_null($args['hugot_id'])){
            $args = Model_Collection_Hugots::add($args);
            return $this->response($args,200);
        }
        return $this->response($args,403);
    }
    
    public function post_remove_huggot()
    {
        $args                           = array();
        $args['huggot_collection_id']   = Input::post('huggot_collection_id',null);
        $args['user_id']                = Session::get('user_id',null);
        
        $is_auth = $this->_is_logged();
        if(!is_null($args['huggot_collection_id']) && $is_auth){
            Model_Collection_Hugots::remove($args);
            return $this->response($args,200);
        }
        return $this->response($args,403);
    }
}
