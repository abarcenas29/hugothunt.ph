<?php
class Controller_Api_Hugot extends Controller_Api_Core
{
    public function post_add()
    {
        $args               = array();
        $args['hugot']      = Input::post('hugot',null);
        $args['url']        = Input::post('url','');
        $args['user_id']    = Session::get('user_id',null);
        
        $is_auth = $this->_is_logged();
        if(!is_null($args['hugot']) && $is_auth){
            $args['hugot_id'] = Model_Hugot_Index::add($args);
            return $this->response($args,200);
        }
        
        $args['error'] = "Missing fields/Invalid request";
        return $this->response($args,403);
    }
    
    public function post_edit()
    {
        $args               = array();
        $args['hugot_id']   = Input::post('hugot_id',null);
        $args['user_id']    = Session::get('user_id',null);
        $args['hugot']      = Input::post('hugot',null);
        $args['url']        = Input::post('url','');
        
        $is_auth = $this->_is_logged();
        if(!is_null($args['hugot_id']) && 
            $is_auth && 
           !is_null($args['hugot'])){
           $args['edit_hugot'] = Model_Hugot_Index::edit($args);
           return $this->response($args,200);
        }

        return $this->response($args,403);
    }
    
    public function post_remove()
    {
        $args               = array();
        $args['user_id']    = Session::get('user_id',null);
        $args['hugot_id']   = Input::post('hugot_id',null);
        
        $is_auth = $this->_is_logged();
        if(!is_null($args['hugot_id']) && $is_auth){
            $args['deleted'] = Model_Hugot_Index::remove($args);
            return $this->response($args,200);
        }
        
        $args['error'] = '';
        return $this->response($args,403);
    }
    
    public function post_comment_add()
    {
        $args               = array();
        $args['hugot_id']   = Input::post('hugot_id',null);
        $args['user_id']    = Session::get('user_id',null);
        $args['comment']    = Input::post('comment',null);
        
        $is_auth = $this->_is_logged();
        if(!is_null($args['hugot_id']) && 
           !empty($args['comment'])    && 
           $is_auth){
           $args = Model_Hugot_Comments::add($args); 
           return $this->response($args,200);
        }
        return $this->response($args,403);
    }
    
    public function post_comment_remove()
    {
        $args                 = array();
        $args['comment_id']   = Input::post('comment_id',null);
        $args['user_id']      = Session::get('user_id',null);
        
        $is_auth = $this->_is_logged();
        if(!is_null($args['comment_id']) && $is_auth){
            Model_Hugot_Comments::remove($args);
            return $this->response($args,200);
        }
        
        return $this->response($args,403);
    }
    
    public function post_comment_edit()
    {
        $args                = array();
        $args['comment_id']  = Input::post('comment_id',null);
        $args['comment']     = Input::post('comment',null);
        $args['user_id']     = Session::get('user_id',null);
        
        $is_auth = $this->_is_logged();
        if(!is_null($args['comment_id']) && 
           !empty($args['comment'])    && 
           $is_auth){
           Model_Hugot_Comments::edit($args);
           return $this->response($args,200);
       }
       return $this->response($args,403);
    }
    
    public function post_vote()
    {
        $args               = array();
        $args['hugot_id']   = Input::post('hugot_id',null);
        $args['user_id']    = Session::get('user_id');
        
        $is_auth = $this->_is_logged();
        if(!is_null($args['hugot_id']) && $is_auth){
            $args = Model_Hugot_Votes::vote($args);
            return $this->response($args,200);
        }
        return $this->response($args,403);
    }
    
    public function post_loop()
    {
        $args           = array();
        $args['page']   = Input::post('page',0);
        
        $result = Model_Hugot_Summary::loop($args);
        return $this->response($result,200);
    }
    
    public function post_detail()
    {
        $args               = array();
        $args['id']   = Input::post('hugot_id',0);
        
        if($args['id'] > 0){
            $result = Model_Hugot_Summary::detail($args);
            return $this->response($result,200);
        }
        return $this->response($args,500);
    }
    
    public function post_view()
    {
        $result = Model_Hugot_Summary::test();
        return $this->response($result,200);
    }
}
