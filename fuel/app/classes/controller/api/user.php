<?php

class Controller_Api_User extends Controller_Api_Core 
{
    /*
     * url:http://hugothung.local(.com in server)/api/user/authenticate.json
     * response:
     *      social_id,
     *      social_netowork,
     *      email
     *      name
     *      id <- user_id in DB
     */
    public function post_authenticate()
    {
        $args                   = array();
        $args['social_id']      = Input::post('social_id',null);
        $args['social_network'] = Input::post('social_network',null);
        $args['email']          = Input::post('email',null);
        $args['name']           = Input::post('name',null);
        
        if(!is_null($this->_is_null($args)))
        {
            $rsp = Model_User_Index::authenticate($args);
            return $this->response($rsp,200);
        }
        return $this->response($args,403);
    }
    
    /*
     * response(array):
     *  [{name,id}]
     */
    public function post_follow_index()
    {
        return $this->response(Model_User_Follows::index(),200);
    }
    
    /*
     * response
     *  user_id
     */
    public function post_follow()
    {
        $args               = array();
        $args['user_follow']= Input::post('user_follow');
        
        $rsp = Model_User_Follows::add($args);
        return $this->response($rsp,200);
    }

    public function post_detail()
    {
        $args               = array();
        $args['user_id']    = Input::post('user_id',0);
        
        if($args['user_id'] > 0){
            $result = array();
            $result = Model_User_Index::detail($args);
            return $this->response($result,200);
        }
        return $this->response($args,500);
    }
    
    /*
     * response
     * true
     */
    public function post_logout()
    {
        return $this->response(Model_User_Index::logout(),200);
    }
}
