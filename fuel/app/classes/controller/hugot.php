<?php

class Controller_Hugot extends Controller_Core
{
    public function action_add()
    {
        $view = View::forge('hugot/add');
        $this->template->content = $view;
    }
    
    public function action_detail($id = null)
    {
        $view = View::forge('hugot/detail');
        $this->template->disable_navigation = 'disable';
        $this->template->content            = $view;
    }
}
