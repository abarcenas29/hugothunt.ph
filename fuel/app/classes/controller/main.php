    <?php
/**
 * Main page to declare the start page for the app
 * 
 * Read Controller Docs here:
 * http://fuelphp.com/docs/general/controllers/base.html
 *
 * @author aldri_000
 */
class Controller_Main extends Controller_Core
{
    /* This can be access in this manner
     * //hugothunt.local/main/index
     * (domain)/(controller)/(action)
     * 
     */
    public function action_index()
    {
        $view = View::forge('main');
        
        $this->template->content = $view;
    }
}
