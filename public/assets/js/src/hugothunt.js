/** @jsx React.DOM */

/*

- this jsx file compiles into plain javascript in the build/ folder
- bash command to instantly compile: jsx --watch src/ build/

==============================
Component Template for ReactJS
------------------------------

var SampleComponent = React.createClass({
    render: function() {
        
        return (
            <div className="sampleComponent">
                Insert to DOM
            </div>
        );
    }
});

==============================

==============================
Structure of Hugot Hunt Feed
------------------------------

- HugotBox
	- HugotSubmitForm
	- HugotList
		- HugotDate
		- HugotEntry
			- HugotUpvote
			- HugotInfo
				- HugotText
				- HugotMeta
					- HugotUser
					- HugotComments
					- HugotAddCollection
		- HugotCollectionBox

==============================


For tutorials on ReactJS, check out:
- http://facebook.github.io/react/docs/tutorial.html
- https://egghead.io/series/react-fundamentals

Creators: Luigi Dollosa, April 2015
For: Hugot Hunt Iteration #1

*/

// var React = require('react');
// var HugotBox = require('HugotBox');

var HugotList = React.createClass({
    render: function() {
    var entries = this.props.data.map(function (entry) {
      return (
        <HugotEntry entry={entry}/>
      );
    });       
        return (
            <div className="hugotList">
 				{entries}
            </div>
        );
    }
});

var HugotEntry = React.createClass({
	render: function() {
		return (
			<a data-toggle="modal" data-target="#modal"><div className="hugotEntry popup-link">Click me
				<div className="hugotUpvote">
					<button type="button" className="btn btnUpvote">
						<i className="fa fa-hand-o-up"></i>
						<h4>{this.props.entry.upvotes}</h4>
					</button>
				</div>
				<div className="hugotDetails">
					<div className="hugotText">{this.props.entry.text}</div>
					<div className="hugotUnder">
						<span><i className="fa fa-user"></i>{this.props.entry.user}</span>
						<span><i className="fa fa-comments"></i>{this.props.entry.comments}</span> 
						<span><a href=""><i className="fa fa-folder-open"></i>Add to your Collection</a></span>
					</div>
				</div>

			</div></a>
		);
	}
});

var data = [
	{
		meme: true,
		date: "April 6, 2015",
  		upvotes: 13,
  		text: "The quick brown fox jumps over the lazy dog.",
  		user_id: 1,
  		user: "Team 404",
  		comments: 25
	},
	{
		meme: false,
		date: "April 6, 2015",
  		upvotes: 14,
  		text: "The quick brown fox jumps over the lazy dog. The quick brown fox jumps over the lazy dog. The quick brown fox jumps over the lazy dog.",
  		user_id: 1,
  		user: "Team 404",
  		comments: 26
	},
];

React.render(
  	<HugotList data={data} />,
  	document.getElementById('feed')
);