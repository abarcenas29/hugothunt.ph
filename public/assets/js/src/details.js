/** @jsx React.DOM */

var HugotDetailsBox = React.createClass({

  componentDidMount: function() {
    this.load();
  },

  getInitialState: function() {
    return {
      data: initialData,
      upvotes: initialData.upvotes,
      comments: initialData.comments,

    }
  },

  load: function() {

    var formData = new FormData();
    formData.append("hugot_id", id);

    $.ajax({
        url: this.props.url,
        data: formData,
        type: 'POST',
        processData: false,
        contentType: false,
        success: function(data) {
          this.setState({data: data});
          this.setState({upvotes: data.upvotes});
        $('[data-toggle="popover"]').popover({
          html: true,
          trigger: 'click',
        });

        }.bind(this),
        error: function(xhr, status, err) {
          console.error(this.props.url, status, err.toString());
        }.bind(this)
    });   
  },

  upvote: function(up) {

    var btnId = "#detail" + id;
    var upvotedStyle = {
            backgroundColor: "#B2EBF2",
            boxShadow: "none",
            color: "#0097A7",
          }
    var unvotedStyle = {
            backgroundColor: "#0097A7",
            boxShadow: "inset 0 -4px #00BCD4",
            color: "#FFFFFF",
          }
    var countUp = this.state.upvotes + 1;
    var countDown = this.state.upvotes - 1;

    var formData = new FormData();
    var entryId = id;

    formData.append('hugot_id', id);

    $.ajax({
          url: "../../api/hugot/vote.json",
          type: 'POST',
          data: formData,
          processData: false,
          contentType: false,       
          success: function(data) {

            var status = data.status;
            if (status === "voted") {
              this.setState({upvotes: countUp});
              this.setState({voted: true});
              $(btnId).css(upvotedStyle);
            }
            else if (status === "not voted") {
              this.setState({upvotes: countDown});
              this.setState({voted: false});
              $(btnId).css(unvotedStyle);
            }   
          }.bind(this),
          error: function(xhr, status, err) {
            console.error(this.props.url, status, err.toString());
          }.bind(this)
    });

  },

  render: function() {
    return (
      <HugotDetails onComment={this.comment} onUpvote={this.upvote} data={this.state.data} upvotes={this.state.upvotes}/>
    );
  }
});

var HugotDetails = React.createClass({

  getInitialState: function() {
    return {
      data: this.props.data,
      comments: this.props.data.comments,
      upvotes: this.props.data.upvotes,
      voted: false,
    };
  },

  comment: function(e) {
    e.preventDefault();
    var comment = this.refs.detailCommentBox.getDOMNode().value.trim();

    var formData = new FormData();
    var hugot_id = this.props.data.id;

    formData.append('hugot_id', id);
    formData.append('comment', comment);

    
    $.ajax({
        url: "../../api/hugot/comment_add.json",
        type: 'POST',
        data: formData,
        processData: false,
        contentType: false,       
        success: function(result) {
          console.log(result);
          // var status = data.status;
          // if (status === "voted") {
          //   this.setState({upvotes: countUp});
          //   this.setState({voted: true});
          //   $(btnId).css(upvotedStyle);
          // }
          // else {
          //   this.setState({upvotes: countDown});
          //   this.setState({voted: false});
          //   $(btnId).css(unvotedStyle);
          // }
          var newComments = this.props.data.comments.push({
                            user_id: result.user_id,
                            pic: result.user_pic,
                            name: result.user_name,
                            comment: result.comment,
                        });
          this.setState({comments: newComments});  
        }.bind(this),
        error: function(xhr, status, err) {
          console.error(this.props.url, status, err.toString());
        }.bind(this)
    });
    e.currentTarget.reset();
  },

  upvote: function(e) {
    e.preventDefault();

    var upvotes = this.state.upvotes;

    this.props.onUpvote({upvotes: upvotes});
    return;

  },

  collect: function() {

  },

	render: function() {
		var upvoters = this.props.data.upvoters.map(function (entry) {
	      	return (
	        	<HugotUpvoter entry={entry}/>
	      	);
	    });

    var collections = this.props.data.collections.map(function (entry) {
      	return (
        	<HugotCollection entry={entry}/>
      	);
    });

    var comments = this.props.data.comments.map(function (entry) {
      	return (
        	<HugotComment entry={entry}/>
      	);
    });

    var redirect = 'http://' + this.props.data.source;

    // has meme? 
    // if (this.props.data.meme == null) {
    //   $('.detailMeme').addClass('invisible');
    // }
    // else {

    // }

    var btnId = "detail" + this.props.data.id;

    var btnClass;
    if (this.props.data.voted == false) {
      btnClass="btn btnUpvote";
    }
    else {
      btnClass="btn upvotedBtn";
    }

    var fbLink = 'https://www.facebook.com/dialog/feed?';
    var fbParams = {
        app_id: 900618836666202,
        name: '"' + this.props.data.text + '"',
        caption: 'mula sa Hugot Hunt',
        picture: this.props.data.meme,
        link: 'http://hugothunt.info/hugot/detail/' + encodeURIComponent(this.props.id),
        redirect_uri: 'http://hugothunt.info'
    };

    var detUrl = "../../user/detail/" + this.props.data.user_id;

		return (
        <div className="modal-content modal-lg modal-details" >
          
          <div className="modal-header">
            <div className="hugotEntry">
              <div className="hugotUpvote">
                <form onSubmit={this.upvote} className="upvoteForm">
                  <button id={btnId} type="submit" className={btnClass}>
                    <i className="fa fa-hand-o-up"></i>
                    <h4>{this.props.upvotes}</h4>
                  </button>
                </form>
              </div>
              <div className="hugotInfo">
                <div className="hugotText">{this.props.data.text}</div>
                <div className="hugotUnder">
                  <span><i className="fa fa-user"></i><a href={detUrl}>{this.props.data.user}</a></span>
                  <span><i className="fa fa-comments"></i>{this.props.data.commentcount}</span> 
                </div>
              </div>
            </div>
          </div>

          <div className="modal-body">
            <div className="hugotDetails">  
              <div className="detailLeft">
                <img className={this.props.data.meme != null ? "detailMeme" : "invisible"} src={this.props.data.meme} />
                <span className="detailTitle">Source: </span><a href={redirect}>{this.props.data.source}</a>
                <span className="detailTitle">Posted On: </span>{this.props.data.date}
                <span className="detailTitle"> Upvoters: </span>
                <div className="hugotUpvoters">           
                  {upvoters.length > 0 ? {upvoters} : "Wala pa :("}
                </div>
                <span className="detailTitle"> Collections part of: </span>
                <div className="hugotCollections">                  
                  {collections.length > 0 ? {collections} : "Wala pa :("}
                </div>
              </div>
              <div className="detailRight">
                <a href={fbLink + $.param(fbParams)} target="_blank"><button><i className="fa fa-share"></i>SHARE</button></a>
                <span className="detailTitle spanComments"> Comments: </span>
                <div className="hugotComments">
                  
                  {comments.length > 0 ? {comments} : "Wala pa :("}
                  <form onSubmit={this.comment} className={signed_in !== null ? 'detailCommentForm' : 'invisible'}>
                    <input className="detailCommentBox" ref="detailCommentBox" type="text" placeholder="Comment here" />
                    <button type="submit"><i className="fa fa-pencil"></i>COMMENT</button>
                  </form>

                </div>
              </div>
            </div>
          </div>

            <div className="modal-footer">
                <button type="button" className="btn btn-default" data-dismiss="modal">Close</button>
            </div>

        </div>
		);
	}
});

var HugotUpvoter = React.createClass({
    render: function() {     
        return (
            <div className="hugotUpvoter">
 				      <img src={this.props.entry.pic} />
            </div>
        );
    }
});

var HugotComment = React.createClass({
    render: function() {
        var userLink = "../../user/detail/" + this.props.entry.id;
        return (
          <div className="hugotComment">
           	<a href={userLink}>
              <img src={this.props.entry.pic} />
            </a>
           	<div>
          		  <a href={userLink}>
                  <span className="hugotCommentName">{this.props.entry.name}</span>
                </a>
					      {this.props.entry.comment}
				    </div>
          </div>
        );
    }
});

var HugotCollection = React.createClass({
    render: function() {
        var defaultImg = "../assets/img/logo-colored.png";
        var url = '../../collections/detail/' + this.props.entry.id;
        return (
            <div className="hugotCollection">
 				       <a href={url}><img src={this.props.entry.photo === null ? defaultImg : this.props.entry.photo} /></a>
            </div>
        );
    }
});

var initialData = {
  //id: 1,
  user: "Loading..",
  upvotes: 0,
  commentcount: "Loading..",
  text: "Loading..",
  meme: "../../assets/img/logo-colored.png",
  source: "Loading..",
  posted: "Loading..",
    upvoters: [
    {
        id: 1,
        pic: "../../assets/img/logo-colored.png"
      }],
    collections: [
    {
        id: 1,
        pic: "../../assets/img/logo-colored.png",
        name: ""
      }],
    comments:[      
      {
        id: 1,
        pic: "../../assets/img/logo-colored.png",
        name: "Loading..",
        comment: "Loading.."
      },
      {
        id: 1,
        pic: "../../assets/img/logo-colored.png",
        name: "Loading..",
        comment: "Loading.."
      }
    ]
}


React.render(
  	<HugotDetailsBox url="../../api/hugot/detail.json" pollInterval={2000}/>,
  	document.getElementById('details')
);