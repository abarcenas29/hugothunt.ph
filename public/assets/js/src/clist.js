/** @jsx React.DOM */

/*

- this jsx file compiles into plain javascript in the build/ folder
- bash command to instantly compile: jsx --watch src/ build/

==============================
Component Template for ReactJS
------------------------------

var SampleComponent = React.createClass({
    render: function() {
        
        return (
            <div className="sampleComponent">
                Insert to DOM
            </div>
        );
    }
});

==============================

==============================
Structure of Hugot Hunt Feed
------------------------------

- HugotBox
	- HugotSubmitForm
	- HugotList
		- HugotDate
		- HugotEntry
			- HugotUpvote
			- HugotInfo
				- HugotText
				- HugotMeta
					- HugotUser
					- HugotComments
					- HugotAddCollection
		- HugotCollectionBox

==============================


For tutorials on ReactJS, check out:
- http://facebook.github.io/react/docs/tutorial.html
- https://egghead.io/series/react-fundamentals

Creators: Luigi Dollosa, April 2015
For: Hugot Hunt Iteration #1

*/

var HugotBox = React.createClass({
    getInitialState: function() {
        return {
        	data: initialData,
          filterText: '',
          collections: [],
        };
    },

    handleUserInput: function(filterText) {
        this.setState({
            filterText: filterText
        });
    },

	loadFeed: function() {
		var formData = new FormData();
    	formData.append("collection_id", col_id);

		$.ajax({
  		url: this.props.url,
  		type: 'POST',
        data: formData,
        processData: false,
        contentType: false,
	  		success: function(data) {
	    		this.setState({data: data});
	    		//console.log("data: " + this.state.data);
	    		$('[data-toggle="popover"]').popover({
	    		 	html: true,
    				trigger: 'click',
	    		});

	    		$('#detailsModal').on('hide.bs.modal', function (e) {
					  window.history.back();
					});

	    		$('.popCheckbox').unbind().click(
	    			function() {
	    				// todo: code for Adding / Removing to collection POST request
	    				var id = $(this).parent().attr('collection-id');
	    				console.log(id);
	    			}
	    		);

	    		$('#nav-main a').unbind().click(
	    			function() {
	    				this.detail();
	    			}
	    		);

		  	}.bind(this),
		  	error: function(xhr, status, err) {
		    	console.error(this.props.url, status, err.toString());
		  	}.bind(this)
		});
	},

	loadCollections: function() {
		$.ajax({
	  		url: "../../api/collection/loop.json",
	  		type: 'POST',
	  		dataType: 'json',
	  		success: function(data) {
	    		this.setState({collections: data});
	    		$('.popCheckbox').change(
	    			function() {
	    				// todo: code for Adding / Removing to collection POST request
	    				var id = $(this).parent().attr('collection-id');
	    				console.log(id);
	    			}
	    		);
		  	}.bind(this),
		  	error: function(xhr, status, err) {
		    	console.error(this.props.url, status, err.toString());
		  	}.bind(this)
		});
	},

	componentDidMount: function() {
		this.loadFeed();
		//console.log("load");
		this.loadCollections();
		setInterval(this.load, this.props.pollInterval);
	},

	upvote: function(entry) {
		console.log("upvote " + entry.id + " " + entry.count);
		var formData = new FormData();
		formData.append('hugot_id', entry.id);		
		$.ajax({
			  url: "../../api/hugot/vote.json",
			  type: 'POST',
			  data: formData,
        processData: false,
        contentType: false,			  
			  success: function(data) {
		    	this.setState({data: data});

		    	console.log("upvote" + this.state.data.id + " " + this.state.data.upvotes);
			  }.bind(this),
			  error: function(xhr, status, err) {
		    	console.error(this.props.url, status, err.toString());
			  }.bind(this)
		});
	},

	collect: function(c) {
		console.log("collection");
	}, 

	detail: function(e) {
		console.log("detail 3");
		React.unmountComponentAtNode(document.getElementById('feed'));
	},

	componentWillUnmount: function(e) {
		// console.log("unmount");
		// React.unmountComponentAtNode(document.getElementById('feed'));
		// window.removeEventListener('click', this.onClick);
		//React.unmountComponentAtNode(document.getElementById('feed'));
		// $('#nav-main a').addEventListener('click', function(){
		// 		console.log("click");
				//this.detail;
		    //document.getElementById("demo").innerHTML = "Hello World";
		// });
	},

	render: function() {
		//console.log(this.state.data);
		return (
			<div className="hugotBox">
				<div className="colhugData">
					<span className="colhugName">{this.state.data.collection_name}</span>
					<span className="colhugDesc">{this.state.data.description}</span>
				</div>

				<HugotSearch filterText={this.state.filterText} onUserInput={this.handleUserInput}/>
				<HugotList onUpvote={this.upvote} data={this.state.data} collections={this.state.collections} filterText={this.state.filterText}/>
 				
 				<div className="modal fade signinPopup" tabindex="-1" role="dialog" aria-hidden="true">
				  <div className="modal-dialog modal-sm">
				    <div className="modal-content">
				    	<div className="modal-header">
				    		Please sign in first to be able to upvote.
				    	</div>
				    	<div className="modal-body">
				    		<a className="btn btn-sm btn-social btn-facebook" href="">
				    			<i className="fa fa-facebook"></i> Sign in with Facebook
				  			</a> or
				  			<a className="btn btn-sm btn-social btn-twitter" href="">
			    				<i className="fa fa-twitter"></i> Sign in with Twitter
			  				</a>
				    	</div>	
				    </div>
				  </div>
				</div>				
			</div>
		);
	}
});

var HugotSearch = React.createClass({
  handleChange: function() {
      this.props.onUserInput(
      	this.refs.filterTextInput.getDOMNode().value
      );
  },

	render: function() {
		return (
			<form>
				<input ref="filterTextInput" className="hugotSearch" type="text" placeholder="Search or filter here..." value={this.props.filterText} onChange={this.handleChange}/>
			</form>
		);
	}
});

var HugotDate = React.createClass({
	render: function() {
		return (
			<span className="hugotDate hugotEntry">{this.props.date}</span>
		);
	}
});

var HugotList = React.createClass({

	onUpvote: function(entry) {
		//console.log("upvote 2 " + entry.id + " " + entry.count);
		this.props.onUpvote({id: entry.id, count: entry.count_votes});
	},
    render: function() {
    	//console.log(this.props.data);
	    var entries = [];
	    var entryVotes = [];
	    var highVote = null;
	    var earlyDate = null;
	    this.props.data.hugots.forEach(function (entry) {
        if (entry.text.toLowerCase().indexOf(this.props.filterText.toLowerCase()) === -1) {
            return;
        }

        // if (entry.upvotes > entryVotes[0]) {
        // 	// console.log("high");
        // 	entries.unshift(<HugotEntry onUpvote={this.onUpvote} entry={entry} key={entry.id} value={entry.upvotes}/>);
        // } 
        // else {
        // 	// console.log("low");
        // 	entries.push(<HugotEntry onUpvote={this.onUpvote} entry={entry} key={entry.id} value={entry.upvotes}/>);
        // }

        // entryVotes.unshift(entry.upvotes);
	    	entries.unshift(<HugotEntry onUpvote={this.onUpvote} collections={this.props.collections} entry={entry} key={entry.id} value={entry.upvotes}/>);

	    	if (entry.posted_time !== earlyDate) {
	    		entries.unshift(<HugotDate date={entry.date} key={entry.date}/>);
	    	}
	    	earlyDate = entry.date;

	    }.bind(this));

	    	 //      	    	entries.sort(function compare(a,b) {
							//   if (a.upvotes < b.upvotes)
							//      return 1;
							//   if (a.upvotes > b.upvotes || a.upvotes === null || b.upvotes === null)
							//     return -1;
							//   return 0;
							// }); 
      
        return (
            <div className="hugotList">               
 							{entries}
            </div>
        );
    }
});

var HugotEntry = React.createClass({
  getInitialState: function() {
      return {
      	count: this.props.entry.upvotes,
      	entry: this.props.entry,
      	url: "",
      	voted: false,
      };
  },

	upvote: function(e) {
		e.preventDefault();
		var id = "#" + this.props.entry.id;
		var upvotedStyle = {
		    		backgroundColor: "#B2EBF2",
		    		boxShadow: "none",
						color: "#0097A7",
		    	}
		var unvotedStyle = {
		    		backgroundColor: "#0097A7",
		    		boxShadow: "inset 0 -4px #00BCD4",
						color: "#FFFFFF",
		    	}
		var countUp = this.state.count + 1;
  	var countDown = this.state.count - 1;

	  var formData = new FormData();
		var entryId = this.props.entry.id;
		formData.append('hugot_id', entryId);

  	$.ajax({
				  url: "../../api/hugot/vote.json",
				  type: 'POST',
				  data: formData,
	        processData: false,
	        contentType: false,			  
				  success: function(data) {
			    	// console.log(data.status);
			    	var status = data.status;
			    	if (status === "voted") {
			    		this.setState({count: countUp});
			    		this.setState({voted: true});
			    		$(id).css(upvotedStyle);
			    	}
			    	else {
			    		this.setState({count: countDown});
			    		this.setState({voted: false});
			    		$(id).css(unvotedStyle);
			    	} 	
				  }.bind(this),
				  error: function(xhr, status, err) {
			    	console.error(this.props.url, status, err.toString());
				  }.bind(this)
		});

	},

	tick: function(e) {
		console.log("tick");
		return;
	},

	detailClick: function(e) {
		var popurl = "../../hugot/detail/" + this.props.entry.id;
		window.history.pushState(null, null, popurl);
		//React.unmountComponentAtNode(document.getElementById('feed'));
	},

	collect: function (e) {
		console.log("collect");
	},

	render: function() {

		var count = this.props.collections;
		if (count === undefined) {
			//console.log("loading..");
		} 
		else {
			var rows = [];

			count.forEach(function(item) {
				rows.push("<div class='popCollect' collection-id='" + item.id + "'>");
				rows.push("<input class='popCheckbox' type='checkbox'/>");
				rows.push("<img src='" + item.meme + "'/>");
				rows.push(item.title);
				rows.push("</div>");
			})
			rows = "<div class='popContainer'>" + 
								rows.join("") + 
								"<span>..or..</span>" + 
								"<button class='popBtn'>" + 
									"<a href='/collections/add'>CREATE A NEW COLLECTION</a>" +
								"</button>" +
							"</div>";
		}

		var popurl = "../../hugot/detail/" + this.props.entry.id;

		var btnId = this.props.entry.id;

    var btnClass;
		if (this.state.entry.voted == false) {
			btnClass="btn btnUpvote";
		}
		else {
			btnClass="btn upvotedBtn";
		}

		var hasMeme;
		if (this.props.entry.meme != null) {
			hasMeme = "(meme inside)";
			//console.log("meme");
		}
		else {
			hasMeme = "";
		}

		var uid = "../../user/detail/" + this.props.entry.user_id;

		return (
			<div className="hugotEntry">
				<div className="hugotUpvote">
					<form className="upvoteForm" onSubmit={this.upvote}>
						<button id={btnId} type="submit" className={btnClass}>
							<i className="fa fa-hand-o-up"></i>
							<h4>{this.state.count}</h4>
						</button>
					</form>
				</div>
				<div className="hugotInfo">
					<a className="hugotText" onClick={this.detailClick} data-toggle="modal" data-target="#detailsModal" data-remote="../../hugot/detail/1">{this.props.entry.text}</a> 
					<span className="hugotMemeMsg">{hasMeme} </span>
					<div className="hugotUnder">
						<span><i className="fa fa-user"></i><a href={uid}>{this.props.entry.user_name}</a></span>
						<span><i className="fa fa-comments"></i>{this.props.entry.comments}</span> 
					</div>
				</div>
			</div>
		);
	}
});

var initialData = {
    collection_name: "sample collection 1",
    collection_id: "2",
    hugots: []
};

React.render(
  	<HugotBox url="../../api/collection/detail.json" pollInterval={10000}/>,
  	document.getElementById('feed')
);