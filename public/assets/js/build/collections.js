/** @jsx React.DOM */

/*

- this jsx file compiles into plain javascript in the build/ folder
- bash command to instantly compile: jsx --watch src/ build/

==============================
Component Template for ReactJS
------------------------------

var SampleComponent = React.createClass({
    render: function() {
        
        return (
            <div className="sampleComponent">
                Insert to DOM
            </div>
        );
    }
});

==============================

==============================
Structure of Hugot Hunt Feed
------------------------------

- HugotBox
	- HugotSubmitForm
	- HugotList
		- HugotDate
		- HugotEntry
			- HugotUpvote
			- HugotInfo
				- HugotText
				- HugotMeta
					- HugotUser
					- HugotComments
					- HugotAddCollection
		- HugotCollectionBox

==============================


For tutorials on ReactJS, check out:
- http://facebook.github.io/react/docs/tutorial.html
- https://egghead.io/series/react-fundamentals

Creators: Luigi Dollosa, April 2015
For: Hugot Hunt Iteration #1

*/

var CollectionList = React.createClass({displayName: "CollectionList",
	
	getInitialState: function () {
		return ({
			data: initialData,
		});
	},

	componentDidMount: function () {
		$.ajax({
      url: this.props.url,
      type: 'POST',
      success: function(data) {
        this.setState({data: data});
      }.bind(this),
      error: function(xhr, status, err) {
        console.error(this.props.url, status, err.toString());
      }.bind(this)
    });
	},
  
  render: function() {
  var entries = this.state.data.map(function (entry) {
    return (
      React.createElement(CollectionEntry, {entry: entry})
    );
  });       
      return (
      	React.createElement("div", null, 
      		React.createElement("button", {className: "collectionCreateBtn"}, React.createElement("a", {href: "add"}, "CREATE A NEW COLLECTION")), 
      		
      		React.createElement("h3", {className: "contentIntro"}, "Mag-enjoy sa napakaraming mga hugot sa loob ng mga collections na ito."), 
        	
        	React.createElement("div", {className: "collectionList"}, 
 						entries != 0 ? entries : "Wala pang collection. Gumawa ka na, please naman."
        	)
      	)
      );
  }
});

var CollectionEntry = React.createClass({displayName: "CollectionEntry",
	render: function() {
		var url = "detail/" + this.props.entry.id;
		var defaultImg = "../assets/img/logo-colored.png";
		return (
			React.createElement("div", {className: "collectionEntry"}, 
				React.createElement("a", {href: url}, 
					React.createElement("img", {src: this.props.entry.meme === undefined ? defaultImg : this.props.entry.meme}), 
					React.createElement("div", {className: "collectionInfo"}, 
						React.createElement("span", {className: "collectionName"}, this.props.entry.title), 						
					React.createElement("div", null, 
							"by: ", React.createElement("img", {src: this.props.entry.user_pic}), 
							this.props.entry.user
						)
					)
				)
			)
		);
	}
});

var picSample = "../assets/img/logo-colored.png";

var initialData = [
	{
		id: 1,
		meme: picSample,
		date: "April 6, 2015",
  		title: "Collection 1",
  		subtitle: "Description description description",
  		hugots: 25,
  		user_id: 1,
  		user_pic: picSample,
  		user: "Team 404",
	}		
];

React.render(
  	React.createElement(CollectionList, {url: "../api/collection/loop.json"}),
  	document.getElementById('feed')
);