/** @jsx React.DOM */

var UserProfile = React.createClass({displayName: "UserProfile",
  componentDidMount: function() {
    this.load();
  },

  getInitialState: function() {
    return {
      data: initialData,
    }
  },

  load: function() {
    //var id = "2";
    var formData = new FormData();
    formData.append("user_id", id);
    console.log(this.props.url);
    $.ajax({
        url: this.props.url,
        data: formData,
        type: 'POST',
        processData: false,
        contentType: false,
        success: function(data) {
            this.setState({data: data});

            if (signed_in === this.state.data.id) {
                $('.submissionText').click(function() {
                     $(this).editable({
                            type: 'textarea',
                            rows: 5,
                            url: "../../api/hugot/edit.json",
                            send: 'always',
                            params: function (params) {
                                var formData = new FormData();
                                var hid = $(this).closest('.userSubmission').attr('data-hugot-id');
                                var hug = params.value;
                                formData.append("hugot_id", hid);
                                formData.append("hugot", hug);
                                return formData;
                            },
                            ajaxOptions: {
                                type: 'POST',
                                processData: false,
                                contentType: false,
                            },
                            success: function(response, newValue) {
                                console.log("what");
                                if(!response.success) return response.msg;
                            },
                            title: "Edit that hugot line."
                        }); 
                });

                $('.collectionTitle').click(function() {
                     $(this).editable({
                            type: 'textarea',
                            rows: 2,
                            url: "../../api/collection/edit.json",
                            send: 'always',
                            params: function (params) {
                                var formData = new FormData();
                                var cid = $(this).closest('.userCollection').attr('data-collection-id');
                                var cug = params.value;
                                formData.append("collection_id", cid);
                                formData.append("name", cug);
                                return formData;
                            },
                            ajaxOptions: {
                                type: 'POST',
                                processData: false,
                                contentType: false,
                            },
                            success: function(response, newValue) {
                                console.log("what");
                                if(!response.success) return response.msg;
                            },
                            title: "Edit that collection name."
                        }); 
                });
            }








        }.bind(this),
        error: function(xhr, status, err) {
          console.error(this.props.url, status, err.toString());
        }.bind(this)
    });   
  },

  follow: function (e) {
    e.preventDefault();
    var uid = this.state.data.id;
    var formData = new FormData();
    formData.append("user_follow", uid);

    var followedStyle = {
            backgroundColor: "#B2EBF2",
            boxShadow: "none",
            color: "#0097A7",
          }
    var unfollowedStyle = {
            backgroundColor: "#0097A7",
            boxShadow: "inset 0 -4px #00BCD4",
            color: "#FFFFFF",
          }
    $.ajax({
        url: "../../api/user/follow.json",
        data: formData,
        type: 'POST',
        processData: false,
        contentType: false,
        success: function(data) {
          if (data.status === "Ok") {
            $('.userFollowBtn').addClass('btn followedBtn');
            $('.userFollowBtn').css(followedStyle);
            //$('.userFollowBtn').removeClass('userFollowBtn');
          }
          if (data.status === "unfollowed") {            
            // $('.userFollowBtn').css(unfollowedStyle);
            $('.userFollowBtn').css(unfollowedStyle);
            $('.userFollowBtn').removeClass('btn followedBtn');
            // $('.userFollowBtn').addClass('userFollowBtn');
            
          }
        }.bind(this),
        error: function(xhr, status, err) {
          console.error(this.props.url, status, err.toString());
        }.bind(this)
    });
  },

	render: function() {

		var submissions = this.state.data.submissions.map(function (entry) {
	      	return (
	        	React.createElement(UserSubmission, {entry: entry})
	      	);
	    });

        var upvoted = this.state.data.upvoted.map(function (entry) {
            return (
                React.createElement(UserUpvote, {entry: entry})
            );
        });

        var collections = this.state.data.collections.map(function (entry) {
            return (
                React.createElement(UserCollection, {entry: entry})
            );
        });

        var followers = this.state.data.followers.map(function (entry) {
            return (
                React.createElement(UserFollower, {entry: entry})
            );
        });

        var followings = this.state.data.following.map(function (entry) {
            return (
                React.createElement(UserFollowing, {entry: entry})
            );
        });

        //var url = window.location.href;
        //var uid = "<?php print Session::get('name') ?>";
        var uname = this.state.data.username.replace(/\s+/g, "").toLowerCase();  
        var email = "mailto:" + this.state.data.email;

        var btnClass;
        if (this.state.data.is_followed === true) {
            btnClass="btn userFollowBtn followedBtn";
        }
        else if (this.state.data.is_followed === false) {
             btnClass="btn userFollowBtn";
        }
        else {
            btnClass="invisible";
        }        

		return (
            React.createElement("div", {className: "userProfile"}, 
                React.createElement("div", {className: "userInfo"}, 
                    React.createElement("form", {onSubmit: this.follow}, 
                        React.createElement("button", {type: "submit", className: btnClass}, React.createElement("i", {className: "fa fa-star-o"}), "FOLLOW")
                    ), 
                    React.createElement("img", {src: this.state.data.social_photo}), 
                    React.createElement("button", {className: "userEditBtn invisible"}, React.createElement("i", {className: "fa fa-pencil"}), "EDIT PROFILE"), 
                    React.createElement("span", {className: "userFullName"}, this.state.data.username), 
                    React.createElement("span", {className: "userName"}, "@", uname), 
                    React.createElement("div", {className: "userSocial"}, 
                        React.createElement("a", {className: "userEmail", href: email}, React.createElement("i", {className: "fa fa-envelope-o"}), "Email"), 
                        React.createElement("a", {className: this.state.data.social_network == "facebook" ? 'userFacebook' : 'invisible', href: this.state.data.social_url}, React.createElement("i", {className: "fa fa-facebook-official"}), "Facebook"), 
                        React.createElement("a", {className: this.state.data.social_network == "twitter" ? 'userTwitter' : 'invisible', href: this.state.data.social_url}, React.createElement("i", {className: "fa fa-twitter-square"}), "Twitter")
                    )
                ), 
                React.createElement("div", {className: "userData"}, 
                    React.createElement("div", {role: "tabpanel"}, 
                      React.createElement("ul", {className: "nav nav-tabs", role: "tablist"}, 
                        React.createElement("li", {role: "presentation", className: "active"}, React.createElement("a", {href: "#submissions", "aria-controls": "submissions", role: "tab", "data-toggle": "tab"}, "SUBMISSIONS")), 
                        React.createElement("li", {role: "presentation"}, React.createElement("a", {href: "#upvotes", "aria-controls": "upvotes", role: "tab", "data-toggle": "tab"}, "UPVOTED")), 
                        React.createElement("li", {role: "presentation"}, React.createElement("a", {href: "#collections", "aria-controls": "collections", role: "tab", "data-toggle": "tab"}, "COLLECTIONS")), 
                        React.createElement("li", {role: "presentation"}, React.createElement("a", {href: "#followers", "aria-controls": "followers", role: "tab", "data-toggle": "tab"}, "FOLLOWERS")), 
                        React.createElement("li", {role: "presentation"}, React.createElement("a", {href: "#following", "aria-controls": "following", role: "tab", "data-toggle": "tab"}, "FOLLOWING"))
                      ), 
                      React.createElement("div", {className: "tab-content"}, 
                        React.createElement("div", {role: "tabpanel", className: "tab-pane active fade in", id: "submissions"}, 
                            React.createElement("div", {className: "userSubmissions"}, 
                                React.createElement("span", {className: "editMsg"}, "Just double click hugot line to edit."), 
                                submissions
                            )
                        ), 
                        React.createElement("div", {role: "tabpanel", className: "tab-pane fade", id: "upvotes"}, 
                            React.createElement("div", {className: "userUpvoted"}, 
                                upvoted
                            )
                        ), 
                        React.createElement("div", {role: "tabpanel", className: "tab-pane fade", id: "collections"}, 
                        React.createElement("span", {className: "editMsg"}, "Just double click title to edit."), 
                            React.createElement("div", {className: "userCollections"}, 
                                collections
                            )
                        ), 
                        React.createElement("div", {role: "tabpanel", className: "tab-pane fade", id: "followers"}, 
                            React.createElement("div", {className: "userFollowers"}, 
                                followers
                            )
                        ), 
                        React.createElement("div", {role: "tabpanel", className: "tab-pane fade", id: "following"}, 
                            React.createElement("div", {className: "userFollowings"}, 
                                followings
                            )
                        )
                      )
                    )
                )
            )
		);
	}
});

var UserSubmission = React.createClass({displayName: "UserSubmission",
    render: function() {    
        return (
            React.createElement("div", {className: "userSubmission", "data-hugot-id": this.props.entry.id}, 
                React.createElement("span", {className: "submissionText"}, this.props.entry.text), 
                React.createElement("div", null, 
                    React.createElement("span", {className: "submissionDetail"}, this.props.entry.upvotes, " upvotes "), 
                    React.createElement("span", {className: "submissionDetail"}, this.props.entry.comments, " comments ")
                )
            )
        );
    }
});

var UserUpvote = React.createClass({displayName: "UserUpvote",
    render: function() {     
        return (
            React.createElement("div", {className: "userUpvote"}, 
                React.createElement("span", {className: "submissionText"}, this.props.entry.text), 
                React.createElement("div", null, 
                    React.createElement("span", {className: "submissionDetail"}, this.props.entry.upvotes, " upvotes "), 
                    React.createElement("span", {className: "submissionDetail"}, this.props.entry.comments, " comments ")
                )
            )
        );
    }
});

var UserCollection = React.createClass({displayName: "UserCollection",
    render: function() {
        var url = "../../collections/detail/" + this.props.entry.id;
        var defaultImg = "../../assets/img/logo-colored.png";
        return (
            React.createElement("div", {className: "userCollection", "data-collection-id": this.props.entry.id}, 
                React.createElement("a", {href: url}, 
                    React.createElement("img", {src: this.props.entry.cover === undefined ? defaultImg : this.props.entry.cover})
                ), 
                React.createElement("span", {className: "collectionTitle"}, this.props.entry.title), 
                React.createElement("span", {className: "collectionDetail"}, this.props.entry.hugots, " hugots")
            )
        );
    }
});

var UserFollower = React.createClass({displayName: "UserFollower",
    render: function() {     
        var url = this.props.entry.id;
        return (
            React.createElement("div", {className: "userFollower"}, 
                React.createElement("img", {src: this.props.entry.pic}), 
                React.createElement("a", {href: url}, this.props.entry.name)
            )
        );
    }
});

var UserFollowing = React.createClass({displayName: "UserFollowing",
    render: function() {     
        var url = "/user/detail/" + this.props.entry.id;
        return (
            React.createElement("div", {className: "userFollowing"}, 
                React.createElement("img", {src: this.props.entry.pic}), 
                React.createElement("a", {href: url}, this.props.entry.name)
            )
        );
    }
});

/* just dummy samples */
var picSample = "../../assets/img/logo-colored.png";
var nameSample = "Team 404";

// {
//     "user_name": "Jay Agonoy",
//     "social_url": "https://facebook.com/7987631432196",
//     "social_photo": "https://graph.facebook.com/7987631432196/picture/redirect=0&type=large",
//     "submissions": [],
//     "upvoted": [],
//     "collections": [],
//     "following": [],
//     "followers": [
//         {
//             "id": "2",
//             "name": "Aldrich Allen Barcenas",
//             "url": "https://facebook.com/7987631432196",
//             "pic": "https://graph.facebook.com/7987631432196/picture/redirect=0&type=large"
//         }
//     ]
// }

var initialData = 
	{	
		id: 1,
  	user_name: "Team 404",
		username: "team404",
		pic: picSample,
		joined: "April 6, 2015",
    email: "mailto:abc@abc.com",
    social_url: "http://twitter.com",
    social_url: "http://facebook.com",
  	submissions: [
  			{
  				id: 1,
  				text: "Sample Hugot 1 Sample Hugot 1 Sample Hugot 1 Sample Hugot 1",
                upvotes: 25,
                comments: 10,
  			},
  			{
                id: 2,
                text: "Sample Hugot 2 Sample Hugot 2 Sample Hugot 2 Sample Hugot 2",
                upvotes: 25,
                comments: 10,
  			},
  			{
                id: 3,
                text: "Sample Hugot 3 Sample Hugot 3 Sample Hugot 3 Sample Hugot 3",
                upvotes: 25,
                comments: 10,
  			},
  			{
                id: 4,
                text: "Sample Hugot 4 Sample Hugot 4 Sample Hugot 4 Sample Hugot 4",
                upvotes: 25,
                comments: 10,
  			},
  			{
                id: 5,
                text: "Sample Hugot 5 Sample Hugot 5 Sample Hugot 5 Sample Hugot 5",
                upvotes: 25,
                comments: 10,
  			},
  		],
		upvoted: [
            {
                id: 1,
                text: "Sample Hugot 1 Sample Hugot 1 Sample Hugot 1 Sample Hugot 1",
                upvotes: 25,
                comments: 10,
            },
            {
                id: 2,
                text: "Sample Hugot 2 Sample Hugot 2 Sample Hugot 2 Sample Hugot 2",
                upvotes: 25,
                comments: 10,
            },
            {
                id: 3,
                text: "Sample Hugot 3 Sample Hugot 3 Sample Hugot 3 Sample Hugot 3",
                upvotes: 25,
                comments: 10,
            },
            {
                id: 4,
                text: "Sample Hugot 4 Sample Hugot 4 Sample Hugot 4 Sample Hugot 4",
                upvotes: 25,
                comments: 10,
            },
            {
                id: 5,
                text: "Sample Hugot 5 Sample Hugot 5 Sample Hugot 5 Sample Hugot 5",
                upvotes: 25,
                comments: 10,
            },
  		],
		collections: [
            {
                id: 1,
                title: "Sample Collection 1",
                hugots: 25,
                cover: picSample,
            },
            {
                id: 2,
                title: "Sample Collection 2",
                hugots: 25,
                cover: picSample,
            },
            {
                id: 3,
                title: "Sample Collection 3",
                hugots: 25,
                cover: picSample,
            },
            {
                id: 4,
                title: "Sample Collection 4",
                hugots: 25,
                cover: picSample,
            },
            {
                id: 5,
                title: "Sample Collection 5",
                hugots: 25,
                cover: picSample,
            },
  		],
    followers: [
            {
                id: 1,
                name: "Sample Follower 1",
                pic: picSample,
            },
            {
                id: 2,
                name: "Sample Follower 2",
                pic: picSample,
            },
            {
                id: 3,
                name: "Sample Follower 3",
                pic: picSample,
            },
            {
                id: 4,
                name: "Sample Follower 4",
                pic: picSample,
            },
            {
                id: 5,
                name: "Sample Follower 5",
                pic: picSample,
            },
        ],
    following: [
            {
                id: 1,
                name: "Sample Following 1",
                pic: picSample,
            },
            {
                id: 2,
                name: "Sample Following 2",
                pic: picSample,
            },
            {
                id: 3,
                name: "Sample Following 3",
                pic: picSample,
            },
            {
                id: 4,
                name: "Sample Following 4",
                pic: picSample,
            },
            {
                id: 5,
                name: "Sample Following 5",
                pic: picSample,
            },
        ],        

	};

React.render(
  	React.createElement(UserProfile, {url: "../../api/user/detail.json"}),
  	document.getElementById('user')
);