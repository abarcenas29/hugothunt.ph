/** @jsx React.DOM */

/*

- this jsx file compiles into plain javascript in the build/ folder
- bash command to instantly compile: jsx --watch src/ build/

==============================
Component Template for ReactJS
------------------------------

var SampleComponent = React.createClass({
    render: function() {
        
        return (
            <div className="sampleComponent">
                Insert to DOM
            </div>
        );
    }
});

==============================

==============================
Structure of Hugot Hunt Feed
------------------------------

- HugotBox
	- HugotSubmitForm
	- HugotList
		- HugotDate
		- HugotEntry
			- HugotUpvote
			- HugotInfo
				- HugotText
				- HugotMeta
					- HugotUser
					- HugotComments
					- HugotAddCollection
		- HugotCollectionBox

==============================


For tutorials on ReactJS, check out:
- http://facebook.github.io/react/docs/tutorial.html
- https://egghead.io/series/react-fundamentals

Creators: Luigi Dollosa, April 2015
For: Hugot Hunt Iteration #1

*/

// var React = require('react');
// var HugotBox = require('HugotBox');
//'use strict';
// var Alert = require('react-bootstrap').OverlayTrigger;
//var Alert = ReactBootstrap.Alert; 
// var OverlayTrigger = ReactBootstrap.OverlayTrigger;
// var Popover = ReactBootstrap.Popover;

// var ProgressBar = ReactBootstrap.ProgressBar;

// var progressInstance = (
//   <ProgressBar now={60} />
// );

// Object.keys(ReactBootstrap).forEach(function (name) { window[name] = ReactBootstrap[name]; });

var HugotBox = React.createClass({displayName: "HugotBox",
    getInitialState: function() {
        return {
        	data: [],
          filterText: '',
          collections: [],
        };
    },

    handleUserInput: function(filterText) {
        this.setState({
            filterText: filterText
        });
    },

	loadFeed: function() {
		$.ajax({
	  		url: this.props.url,
	  		type: 'POST',
	  		dataType: 'json',
	  		success: function(data) {
	    		this.setState({data: data});
	    		
	    		$('[data-toggle="popover"]').popover({
	    		 	html: true,
    				trigger: 'click',
	    		});

	    		$('#detailsModal').on('hide.bs.modal', function (e) {
					  window.history.back();
					});

					$(document).on('click', '.popCheckbox', function () {

					  	var col_id = $(this).parent().attr('collection-id');
					  	var hugot_id = $(this).parent().attr('h-id');
					  	// console.log("hid: " + hugot_id + " cid: " + col_id);

	    				var formData = new FormData();
							formData.append('collection_id', col_id);
							formData.append('hugot_id', hugot_id);

	    				$.ajax({
						  		url: "api/collection/add_hugot.json",
						  		type: 'POST',
						  		data: formData,
						  		processData: false,
        					contentType: false,	
						  		success: function(data) {
						  			console.log("success");
							  	}.bind(this),
							  	error: function(xhr, status, err) {
							    	console.error(this.props.url, status, err.toString());
							  	}.bind(this)
							});

					});

	    		$('#nav-main a').unbind().click(
	    			function() {
	    				this.detail();
	    			}
	    		);

		  	}.bind(this),
		  	error: function(xhr, status, err) {
		    	console.error(this.props.url, status, err.toString());
		  	}.bind(this)
		});
	},

	loadCollections: function() {
		$.ajax({
	  		url: "api/collection/loop.json",
	  		type: 'POST',
	  		dataType: 'json',
	  		success: function(data) {
	    		this.setState({collections: data});
	    		$('.popCheckbox').change(
	    			function() {
	    				// todo: code for Adding / Removing to collection POST request
	    				var id = $(this).parent().attr('collection-id');
	    				console.log(id);
	    			}
	    		);
		  	}.bind(this),
		  	error: function(xhr, status, err) {
		    	console.error(this.props.url, status, err.toString());
		  	}.bind(this)
		});
	},

	componentDidMount: function() {
		this.loadFeed();
		// this.loadCollections();
		setInterval(this.load, this.props.pollInterval);
	},

	upvote: function(entry) {

		if (signed_in = null) {
			console.log("not signed in");
		}

		console.log("upvote " + entry.id + " " + entry.count);
		var formData = new FormData();
		formData.append('hugot_id', entry.id);		
		$.ajax({
			  url: "api/hugot/vote.json",
			  type: 'POST',
			  data: formData,
        processData: false,
        contentType: false,			  
			  success: function(data) {
		    	this.setState({data: data});
		    	console.log("upvote" + this.state.data.id + " " + this.state.data.upvotes);
			  }.bind(this),
			  error: function(xhr, status, err) {
		    	console.error(this.props.url, status, err.toString());
			  }.bind(this)
		});
	},

	collect: function(c) {
		console.log("collection");
	}, 

	detail: function(e) {
		console.log("detail 3");
		React.unmountComponentAtNode(document.getElementById('feed'));
	},

	componentWillUnmount: function(e) {

	},

	render: function() {
		//console.log(this.state.data);
		return (
			React.createElement("div", {className: "hugotBox"}, 
				React.createElement(HugotSearch, {filterText: this.state.filterText, onUserInput: this.handleUserInput}), 
				React.createElement(HugotList, {onUpvote: this.upvote, data: this.state.data, collections: this.state.data.my_collections, filterText: this.state.filterText})	
			)
		);
	}
});

var HugotSearch = React.createClass({displayName: "HugotSearch",
  handleChange: function() {
      this.props.onUserInput(
      	this.refs.filterTextInput.getDOMNode().value
      );
  },

	render: function() {
		return (
			React.createElement("form", null, 
				React.createElement("input", {ref: "filterTextInput", className: "hugotSearch", type: "text", placeholder: "Search or filter here...", value: this.props.filterText, onChange: this.handleChange})
			)
		);
	}
});

var HugotDate = React.createClass({displayName: "HugotDate",
	render: function() {
		return (
			React.createElement("span", {className: "hugotDate hugotEntry"}, this.props.date)
		);
	}
});

var HugotList = React.createClass({displayName: "HugotList",

	onUpvote: function(entry) {
		//console.log("upvote 2 " + entry.id + " " + entry.count);
		this.props.onUpvote({id: entry.id, count: entry.count_votes});
	},
    render: function() {
	    var entries = [];
	    var entryVotes = [];
	    var highVote = null;
	    var earlyDate = null;
	    this.props.data.forEach(function (entry) {
        if (entry.text.toLowerCase().indexOf(this.props.filterText.toLowerCase()) === -1) {
            return;
        }

        // if (entry.upvotes > entryVotes[0]) {
        // 	// console.log("high");
        // 	entries.unshift(<HugotEntry onUpvote={this.onUpvote} entry={entry} key={entry.id} value={entry.upvotes}/>);
        // } 
        // else {
        // 	// console.log("low");
        // 	entries.push(<HugotEntry onUpvote={this.onUpvote} entry={entry} key={entry.id} value={entry.upvotes}/>);
        // }

        // entryVotes.unshift(entry.upvotes);
	    	
	    	if (entry.date !== earlyDate) {
	    		entries.push(React.createElement(HugotDate, {date: entry.date, key: entry.date}));
	    	}

	    	entries.push(React.createElement(HugotEntry, {onUpvote: this.onUpvote, collections: this.props.collections, entry: entry, key: entry.id, value: entry.upvotes}));

	    	earlyDate = entry.date;

	    }.bind(this));

	    	 //      	    	entries.sort(function compare(a,b) {
							//   if (a.upvotes < b.upvotes)
							//      return 1;
							//   if (a.upvotes > b.upvotes || a.upvotes === null || b.upvotes === null)
							//     return -1;
							//   return 0;
							// }); 
      
        return (
            React.createElement("div", {className: "hugotList"}, 
 							entries
            )
        );
    }
});

var HugotEntry = React.createClass({displayName: "HugotEntry",
  getInitialState: function() {
      return {
      	count: this.props.entry.upvotes,
      	entry: this.props.entry,
      	url: "",
      	voted: false,
      };
  },

	upvote: function(e) {
		e.preventDefault();
		var id = "#" + this.props.entry.id;
		var upvotedStyle = {
		    		backgroundColor: "#B2EBF2",
		    		boxShadow: "none",
						color: "#0097A7",
		    	}
		var unvotedStyle = {
		    		backgroundColor: "#0097A7",
		    		boxShadow: "inset 0 -4px #00BCD4",
						color: "#FFFFFF",
		    	}
		var countUp = this.state.count + 1;
  	var countDown = this.state.count - 1;

	  var formData = new FormData();
		var entryId = this.props.entry.id;
		formData.append('hugot_id', entryId);

  	$.ajax({
				  url: "api/hugot/vote.json",
				  type: 'POST',
				  data: formData,
	        processData: false,
	        contentType: false,			  
				  success: function(data) {
			    	// console.log(data.status);
			    	var status = data.status;
			    	if (status === "voted") {
			    		this.setState({count: countUp});
			    		this.setState({voted: true});
			    		$(id).css(upvotedStyle);
			    	}
			    	else {
			    		this.setState({count: countDown});
			    		this.setState({voted: false});
			    		$(id).css(unvotedStyle);
			    	} 	
				  }.bind(this),
				  error: function(xhr, status, err) {
				  		$('.signinPopup').modal('show');
			    	console.error(this.props.url, status, err.toString());
				  }.bind(this)
		});

	},

	tick: function(e) {
		console.log("tick");
		return;
	},

	detailClick: function(e) {
		var popurl = "hugot/detail/" + this.props.entry.id;
		window.history.pushState(null, null, popurl);

		$("#detailsModal").modal();
		$("#detailsModal").load(popurl);
		console.log("popopopop");
	},

	collect: function (e) {
		console.log("collect");
	},

	render: function() {

		var count = this.state.entry;
		//var hugot_id = this.state.entry.id;
		if (count === undefined) {
			//console.log("loading..");
		} 
		else {
			var rows = [];

			count.my_collections.forEach(function(item) {
				
				var checked;
				if (item.added === true) {
					checked = "checked";
				} 
				else { checked = ""; }

				var cover;
				if (item.photo === undefined) {
					cover = "invisible";
				}
				rows.push("<div class='popCollect' h-id='" + count.id + "' collection-id='" + item.id + "'>");
				rows.push("<input class='popCheckbox' type='checkbox'" + checked + "/>");
				rows.push("<img src='" + item.photo + "' class='" + cover + "'/>");
				rows.push("<a target='_blank' href='/collections/detail/" + item.id + "'>");
				rows.push(item.name);
				rows.push("</a>");
				rows.push("</div>");
			})
			rows = "<div class='popContainer'>" + 
								rows.join("") + 
								"<span>..or..</span>" + 
								"<button class='popBtn'>" + 
									"<a href='/collections/add'>CREATE A NEW COLLECTION</a>" +
								"</button>" +
							"</div>";
		}

		var popurl = "hugot/detail/" + this.props.entry.id;

		var btnId = this.props.entry.id;

    var btnClass;
		if (this.state.entry.voted == false) {
			btnClass="btn btnUpvote"
		}
		else {
			btnClass="btn upvotedBtn"
		}

		var hasMeme;
		if (this.props.entry.meme != null) {
			hasMeme = "(meme inside)";
			//console.log("meme");
		}
		else {
			hasMeme = "";
		}

		var uid = "user/detail/" + this.props.entry.user_id;

		return (
			React.createElement("div", {className: "hugotEntry"}, 
				React.createElement("div", {className: "hugotUpvote"}, 
					React.createElement("form", {className: "upvoteForm", onSubmit: this.upvote}, 
						React.createElement("button", {id: btnId, type: "submit", className: btnClass}, 
							React.createElement("i", {className: "fa fa-hand-o-up"}), 
							React.createElement("h4", null, this.state.count)
						)
					)
				), 
				React.createElement("div", {className: "hugotInfo"}, 
					React.createElement("a", {className: "hugotText", onClick: this.detailClick, "data-toggle": "modal"}, this.props.entry.text), 
					React.createElement("span", {className: "hugotMemeMsg"}, hasMeme, " "), 
					React.createElement("div", {className: "hugotUnder"}, 
						React.createElement("span", null, React.createElement("i", {className: "fa fa-user"}), React.createElement("a", {href: uid}, this.props.entry.user_name)), 
						React.createElement("span", null, React.createElement("i", {className: "fa fa-comments"}), this.props.entry.comments), 
						React.createElement("span", null, 
								
								React.createElement("a", {className: "popCollection", tabindex: "0", role: "button", 
									"data-toggle": "popover", "data-trigger": "click", "data-content": rows
								}, 								
									React.createElement("i", {className: "fa fa-folder-open"}), 
									"Add to your Collection"

								)
						)
					)
				)
			)
		);
	}
});

React.render(
  	React.createElement(HugotBox, {url: "api/hugot/loop.json", pollInterval: 2000}),
  	document.getElementById('feed')
);