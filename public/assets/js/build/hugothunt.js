/** @jsx React.DOM */

/*

- this jsx file compiles into plain javascript in the build/ folder
- bash command to instantly compile: jsx --watch src/ build/

==============================
Component Template for ReactJS
------------------------------

var SampleComponent = React.createClass({
    render: function() {
        
        return (
            <div className="sampleComponent">
                Insert to DOM
            </div>
        );
    }
});

==============================

==============================
Structure of Hugot Hunt Feed
------------------------------

- HugotBox
	- HugotSubmitForm
	- HugotList
		- HugotDate
		- HugotEntry
			- HugotUpvote
			- HugotInfo
				- HugotText
				- HugotMeta
					- HugotUser
					- HugotComments
					- HugotAddCollection
		- HugotCollectionBox

==============================


For tutorials on ReactJS, check out:
- http://facebook.github.io/react/docs/tutorial.html
- https://egghead.io/series/react-fundamentals

Creators: Luigi Dollosa, April 2015
For: Hugot Hunt Iteration #1

*/

// var React = require('react');
// var HugotBox = require('HugotBox');

var HugotList = React.createClass({displayName: "HugotList",
    render: function() {
    var entries = this.props.data.map(function (entry) {
      return (
        React.createElement(HugotEntry, {entry: entry})
      );
    });       
        return (
            React.createElement("div", {className: "hugotList"}, 
 				entries
            )
        );
    }
});

var HugotEntry = React.createClass({displayName: "HugotEntry",
	render: function() {
		return (
			React.createElement("a", {"data-toggle": "modal", "data-target": "#modal"}, React.createElement("div", {className: "hugotEntry popup-link"}, "Click me", 
				React.createElement("div", {className: "hugotUpvote"}, 
					React.createElement("button", {type: "button", className: "btn btnUpvote"}, 
						React.createElement("i", {className: "fa fa-hand-o-up"}), 
						React.createElement("h4", null, this.props.entry.upvotes)
					)
				), 
				React.createElement("div", {className: "hugotDetails"}, 
					React.createElement("div", {className: "hugotText"}, this.props.entry.text), 
					React.createElement("div", {className: "hugotUnder"}, 
						React.createElement("span", null, React.createElement("i", {className: "fa fa-user"}), this.props.entry.user), 
						React.createElement("span", null, React.createElement("i", {className: "fa fa-comments"}), this.props.entry.comments), 
						React.createElement("span", null, React.createElement("a", {href: ""}, React.createElement("i", {className: "fa fa-folder-open"}), "Add to your Collection"))
					)
				)

			))
		);
	}
});

var data = [
	{
		meme: true,
		date: "April 6, 2015",
  		upvotes: 13,
  		text: "The quick brown fox jumps over the lazy dog.",
  		user_id: 1,
  		user: "Team 404",
  		comments: 25
	},
	{
		meme: false,
		date: "April 6, 2015",
  		upvotes: 14,
  		text: "The quick brown fox jumps over the lazy dog. The quick brown fox jumps over the lazy dog. The quick brown fox jumps over the lazy dog.",
  		user_id: 1,
  		user: "Team 404",
  		comments: 26
	},
];

React.render(
  	React.createElement(HugotList, {data: data}),
  	document.getElementById('feed')
);