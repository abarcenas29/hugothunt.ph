#HugotHunt.ph

## Description

A place where all only the best #Hugot lines gets the spotlight

## Development Team (Team 404)

* Aldrich Allen Barcenas - Back-end Developer
* Luigi Dollosa          - Front-end Developer
* Alexis Acosta          - Back-end Developer

## Fuel Installation Instructions

### Pre-requisite
* XAMPP (or any kind of Apache server)
* MySQL <- This is already included in XAMPP package
* PHP 5.3 and Above <- This is already included in XAMPP package
* You can setup vhost

### Setup

#### Virtual-Host Config
* Go to your vhost file (it's in "xampp/apache/conf/extra/http-vhost.conf")
* paste this text: http://pastebin.com/pSkFs0D9
* change the Document Root and the Directory path to your installation

#### Host File Setup
* Open Notepad on Admin Right (Run as Administrator)
* Go to windows/system32/drivers/etc/hosts. (Make sure All Files are shown)
* Follow the format written, type 127.0.0.1 hugothunt.local
* If XAMPP is running, restart it to reflect the changes

## Notes
* The setup is for Windows PC. It's tendious to write instructions for the MAC.
